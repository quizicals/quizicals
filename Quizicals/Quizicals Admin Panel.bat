@echo off

title Quizicals Admin Panel

:menu
echo 1. Execute Queries
echo 2. Change properties
echo 3. Open database
echo 4. Exit
set /p selection=Enter a selection:  

cls

IF %selection%==1 (
	goto query
) ELSE IF %selection%==2 (
	goto properties
) ELSE IF %selection%==3 (
	start http://localhost/phpmyadmin/
	goto end
) ELSE IF %selection%==4 (
	goto end
) ELSE (
	goto menu
)

:query
	java -jar Quizicals.jar -q
	goto end

:properties
	java -jar Quizicals.jar -p

:end