package exceptions;

/**
 * Thrown when the question string is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullQuestionStringException extends RuntimeException
{
}	//end of NullQuestionStringException