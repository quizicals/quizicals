package exceptions;

/**
 * Thrown when a true and false question does 
 * not contain true and false, or it does not 
 * have true first followed by false, or there 
 * are more than 2 possible answers.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidTFQuestionException extends RuntimeException
{
}	//end of InvalidTFQuestionException class