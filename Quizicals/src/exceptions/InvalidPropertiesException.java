package exceptions;

/**
 * Thrown when the amount of properties is invalid.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidPropertiesException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the sizes were.
	 * 
	 * @param size			Holds the given number of properties.
	 * @param numProperties	Holds the valid number of properties.
	 */
	public InvalidPropertiesException(int size, int numProperties)
	{
		super(size + " != " + numProperties);
	}	//end of InvalidPropertiesException constructor
}	//end of InvalidPropertiesException class