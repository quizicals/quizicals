package exceptions;

/**
 * Thrown when the question list is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullQuestionListException extends RuntimeException
{
}	//end of NullQuestionListException class