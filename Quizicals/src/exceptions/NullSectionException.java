package exceptions;

/**
 * Thrown when the quiz section is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullSectionException extends RuntimeException
{
}	//end of NullSectionException class