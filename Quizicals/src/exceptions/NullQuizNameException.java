package exceptions;

/**
 * Thrown when the quiz name is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullQuizNameException extends RuntimeException
{
}	//end of NullQuizNameException class