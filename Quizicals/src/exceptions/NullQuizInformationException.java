package exceptions;

/**
 * Thrown when the quiz information is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullQuizInformationException extends RuntimeException
{
}	//end of NullQuizInformationException class