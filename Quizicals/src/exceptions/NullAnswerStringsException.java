package exceptions;

/**
 * Thrown when the answer strings list is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullAnswerStringsException extends RuntimeException
{
}	//end of NullAnswerStringsException class