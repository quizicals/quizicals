package exceptions;

/**
 * Thrown when the quiz scores list is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullQuizScoresListException extends RuntimeException
{
}	//end of NullQuizScoresListException class