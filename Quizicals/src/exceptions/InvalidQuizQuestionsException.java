package exceptions;

/**
 * Thrown when the number of questions in the 
 * quiz does not equal the number of questions 
 * in the quiz information.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidQuizQuestionsException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the 
	 * sizes were.
	 * 
	 * @param questionsSize		Holds the amount of questions the quiz has.
	 * @param informationSize	Holds the amount of questions the information has.
	 */
	public InvalidQuizQuestionsException(int questionsSize, int informationSize)
	{
		super(questionsSize + " != " + informationSize);
	}	//end of InvalidQuizQuestionsException constructor
}	//end of InvalidQuizQuestionsException class