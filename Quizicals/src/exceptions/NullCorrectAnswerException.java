package exceptions;

/**
 * Thrown when the correct answer is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullCorrectAnswerException extends RuntimeException
{
}	//end of NullCorrectAnswerException class