package exceptions;

/**
 * Thrown when the given index is out of range.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidPropertyException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the property index was.
	 * 
	 * @param index	Holds the index of the property.
	 */
	public InvalidPropertyException(int index)
	{
			super("property=" + index);
	}	//end of InvalidPropertyException constructor
}	//end of InvalidPropertyException class