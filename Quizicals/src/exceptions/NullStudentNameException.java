package exceptions;

/**
 * Thrown when the student name is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullStudentNameException extends RuntimeException
{
}	//end of NullStudentNameException class