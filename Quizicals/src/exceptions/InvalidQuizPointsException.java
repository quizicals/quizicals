package exceptions;

/**
 * Thrown when the sum of the points for each 
 * question does not equal the total amount of points 
 * in the quiz information.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidQuizPointsException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the 
	 * points were.
	 * 
	 * @param questionsPoints	Holds the amount of points the quiz has.
	 * @param informationPoints	Holds the amount of points the information has.
	 */
	public InvalidQuizPointsException(int questionsPoints, int informationPoints)
	{
		super(questionsPoints + " != " + informationPoints);
	}	//end of InvalidQuizPointsException constructor
}	//end of InvalidQuizPointsException class