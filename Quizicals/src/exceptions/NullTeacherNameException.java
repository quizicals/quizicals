package exceptions;

/**
 * Thrown when the teacher name is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullTeacherNameException extends RuntimeException
{
}	//end of NullTeacherNameException class