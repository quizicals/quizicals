package exceptions;

/**
 * Thrown when the amount of points is invalid.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidPointsException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the 
	 * points were.
	 * 
	 * @param points	Holds the amount of points the quiz is worth.
	 */
	public InvalidPointsException(int points)
	{
		super("points=" + points);
	}	//end of InvalidPointsException constructor
}	//end of InvalidPointsException class