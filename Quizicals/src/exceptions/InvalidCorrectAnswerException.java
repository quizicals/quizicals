package exceptions;

/**
 * Thrown when the correct answer is not in the 
 * list of possible answers.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidCorrectAnswerException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the 
	 * correct answer was.
	 * 
	 * @param correctAnswer	Holds the correct answer.
	 */
	public InvalidCorrectAnswerException(String correctAnswer)
	{
		super("correctAnswer=" + correctAnswer);
	}	//end of InvalidCorrectAnswerException constructor
}	//end of InvalidCorrectAnswerException class