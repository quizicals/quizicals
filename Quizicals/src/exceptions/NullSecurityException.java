package exceptions;

/**
 * Thrown when a parameter passed to encrypt or 
 * decrypt is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullSecurityException extends RuntimeException
{
}	//end of NullSecurityException