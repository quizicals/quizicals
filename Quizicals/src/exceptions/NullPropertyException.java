package exceptions;

/**
 * Thrown when a null property is given.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullPropertyException extends RuntimeException
{
}	//end of NullPropertyException class