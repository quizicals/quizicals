package exceptions;

/**
 * Thrown when the number of questions is invalid.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidQuestionsException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays what the 
	 * number of questions were.
	 * 
	 * @param points	Holds the number of questions the quiz contains.
	 */
	public InvalidQuestionsException(int numberOfQuestions)
	{
		super("numberOfQuestions=" + numberOfQuestions);
	}	//end of InvalidQuestionsException constructor
}	//end of InvalidQuestionsException class