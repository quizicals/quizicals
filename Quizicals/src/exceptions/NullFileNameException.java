package exceptions;

/**
 * Thrown when the file name is null.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class NullFileNameException extends RuntimeException
{
}	//end of NullFileNameException class