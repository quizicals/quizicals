package exceptions;

import java.util.LinkedList;

/**
 * Thrown when the answers string has an invalid size.
 * 
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class InvalidAnswersException extends RuntimeException
{
	/**
	 * Throws a runtime exception and displays how many
	 * answers there were.
	 * 
	 * @param answerStrings	Holds a list of possible answers.
	 */
	public InvalidAnswersException(LinkedList<String> answerStrings)
	{
		super("answerStringsSize=" + answerStrings.size());
	}	//end of InvalidAnswersException constructor
}	//end of InvalidAnswersException class