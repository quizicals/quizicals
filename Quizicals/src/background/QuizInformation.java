package background;

import java.io.Serializable;

import exceptions.InvalidPointsException;
import exceptions.InvalidQuestionsException;
import exceptions.NullQuizNameException;
import exceptions.NullTeacherNameException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class QuizInformation implements Serializable
{
	/**
	 * Holds the name of the quiz.
	 */
	private final String quizName;
	
	/**
	 * Holds the name of the teacher.
	 */
	private final String teacherName;
	
	/**
	 * Holds the amount of points the quiz 
	 * is worth.
	 */
	private final int totalPoints;
	
	/**
	 * Holds the number of questions the 
	 * quiz contains.
	 */
	private final int numberOfQuestions;
	
	/**
	 * Constructor initializes fields.
	 * 
	 * @param quizName			Holds the name of the quiz.
	 * @param teacherName		Holds the name of the teacher.
	 * @param totalPoints		Holds the amount of points the quiz is worth.
	 * @param numberOfQuestions	Holds the number of questions the quiz contains.
	 */
	public QuizInformation(String quizName, String teacherName, int totalPoints, int numberOfQuestions)
	{
		//check if the quiz name is null
		if (quizName == null)	//the quiz name is null
		{
			throw new NullQuizNameException();
		}	//end if
		
		//check if the teacher name is null
		if (teacherName == null)	//the teacher name is null
		{
			throw new NullTeacherNameException();
		}	//end if
		
		//check if the number of points is valid
		if (totalPoints <= 0 || totalPoints > Constants.MAX_POINTS)	//the number of points is invalid
		{
			throw new InvalidPointsException(totalPoints);
		}	//end if
		
		//check if the number of questions is valid
		if (numberOfQuestions <= 0 || numberOfQuestions > Constants.MAX_QUESTIONS)	//the number of questions is invalid
		{
			throw new InvalidQuestionsException(numberOfQuestions);
		}	//end if
		
		//set fields
		this.quizName = quizName;
		this.teacherName = teacherName;
		this.totalPoints = totalPoints;
		this.numberOfQuestions = numberOfQuestions;
	}	//end of QuizInformation constructor
	
	/**
	 * Returns the name of the quiz.
	 * 
	 * @return	Returns the name of the quiz.
	 */
	public String getQuizName()
	{
		return this.quizName;
	}	//end of getQuizName method
	
	/**
	 * Returns the name of the teacher.
	 * 
	 * @return	Returns the name of the teacher.
	 */
	public String getTeacherName()
	{
		return this.teacherName;
	}	//end of getTeacherName method
	
	/**
	 * Returns the amount of points the quiz is worth.
	 * 
	 * @return	Returns the amount of points the quiz is worth.
	 */
	public int getTotalPoints()
	{
		return this.totalPoints;
	}	//end of getTotalPoints method
	
	/**
	 * Returns the number of questions the quiz contains.
	 * 
	 * @return	Returns the number of questions the quiz contains.
	 */
	public int getNumberOfQuestions()
	{
		return this.numberOfQuestions;
	}	//end of getNumberOfQuestions method
}	//end of QuizInformation class