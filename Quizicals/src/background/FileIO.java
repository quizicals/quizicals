package background;

import java.io.*;

public class FileIO {
	
	/**
	 * Static methods only, does not need instantiated.
	 */
	private FileIO(String fileName) throws IOException { } 
	
	/**
	 * Find a file. This will search the file share for the requested file.
	 * A file object pointing to the requested file will be returned. The calling function is responsible for reading and
	 * decrypting the file.
	 * @return A file object 
	 */
	public static Object read(String fileName) {
		/**
		 * TODO: read byte[] array from file
		 * decrypt byte[] array to String
		 * String to decryoted byte[] array
		 * byte[] array to object
		 */
		//If we haven't looked at what the share is, read what the share is and open it
		String shareDirectory = Properties.getProperty(Properties.DIRECTORY);
		if(shareDirectory != "" && (shareDirectory.charAt(shareDirectory.length() - 1) != '/' || shareDirectory.charAt(shareDirectory.length() - 1) != '\\'))
		{
			shareDirectory += "/";
		}
		/*if (System.getProperty("os.name").startsWith("Windows") && shareDirectory.charAt(shareDirectory.length() - 1) != '\\') {
			shareDirectory += "\\"; //Need a backslash at the end for simplicity sake
		}
		else if (!System.getProperty("os.name").startsWith("Windows") && shareDirectory.charAt(shareDirectory.length() - 1) != '/') {
			shareDirectory += "/"; //Mac and Linux use / for directory traversal
		}*/
		
		StringBuilder sb = new StringBuilder();
		sb.append(shareDirectory);
		
		if (System.getProperty("os.name").startsWith("Windows")) sb.append(fileName.replace("/", "\\")); //Windows convention is \ in directory path
		else sb.append(fileName);
		File f = new File(sb.toString());
		if (!f.exists()) return null;
		byte[] encryptedContents = new byte[(int) f.length()]; //TODO fix for file > 4GB
		try {
			FileInputStream inputStream = new FileInputStream(f);
			inputStream.read(encryptedContents);
			inputStream.close();
		} 
		catch (IOException e) { 
			//The file cannot be read, most likely due to permissions
			//TODO what should I do in this case?
		}
		byte[] decryptedContents = SecurityManager.decrypt(encryptedContents);
		ByteArrayInputStream byteInputStream = new ByteArrayInputStream(decryptedContents);
		try {
			ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
			Object o = objectInputStream.readObject();
			objectInputStream.close();
			return o;
		}
		catch (ClassNotFoundException e) {
			//Abandon hope yee who arrive here
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
		}
		
		return null; //TODO is it better to throw an exception here if we can't find it?
	}
	
	/**
	 * Write a file. The file will be encrypted.
	 * TODO Work out the parameters of this function, also race conditions exist if multiple want to write to single file
	 * really bad if it's the database file
	 */
	public static void write(String fileName, Object contents) {
		String shareDirectory = Properties.getProperty(Properties.DIRECTORY);
		/*if (System.getProperty("os.name").startsWith("Windows") && shareDirectory.length() != 0 && shareDirectory.charAt(shareDirectory.length() - 1) != '\\') {
			shareDirectory += "\\"; //Need a backslash at the end for simplicity sake
		}
		else if (!System.getProperty("os.name").startsWith("Windows") && shareDirectory.length() != 0 && shareDirectory.charAt(shareDirectory.length() - 1) != '/') {
			shareDirectory += "/"; //Mac and Linux use / for directory traversal
		}*/
		if(shareDirectory != "" && (shareDirectory.charAt(shareDirectory.length() - 1) != '/' || shareDirectory.charAt(shareDirectory.length() - 1) != '\\'))
		{
			shareDirectory += "/";
		}
		
		
		StringBuilder sb = new StringBuilder();
		sb.append(shareDirectory);
		/*if (System.getProperty("os.name").startsWith("Windows")) sb.append(fileName.replace("/", "\\")); //Windows convention is \ in directory path
		else*/ sb.append(fileName);
		
		ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
		byte[] unencryptedContents;
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
			objectOutputStream.writeObject(contents);
			unencryptedContents = byteOutputStream.toByteArray();
			objectOutputStream.close();
		}
		catch (IOException e) {
			return; //TODO SOMETHING
		}
		byte[] encryptedContents = SecurityManager.encrypt(unencryptedContents);
		try {
			
			if (fileName.contains("/")) {
				String s = fileName.substring(0, fileName.lastIndexOf('/'));
				s = shareDirectory + s;
				File f = new File(s);
				if (!f.exists()) {
					f.mkdirs();
				}
			}

			FileOutputStream fileOutput = new FileOutputStream(sb.toString());
			fileOutput.write(encryptedContents);
			fileOutput.close();
		}
		catch (IOException e) {
		}
	}
	
	/**
	 * Initialize FileIO to know where the share is. We are holding the share file in the local directory
	 * of the current machine for two main reasons. 1.) Each operating system can set this file up in the 
	 * syntax of the way file shares work. 2.) This makes the application a bit more portable for deployment. 
	 * We also avoid encryption with this file because it should be editable from outside the program
	 * for ease of use by IT
	 */
	/*private static synchronized void init() {
		
		File shareFile = new File(SHARE_FILE);
		if(!shareFile.exists()) {
			//File doesn't exist. Something is wrong.
		}
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(shareFile));
			shareDirectory = br.readLine(); //This file should be only a single line. If it's not, someone tampered with the file and we can't do anything
			br.close();
		}
		catch (IOException e) {
			//either the file is not found or can't be read for whatever reason
			//TODO alert the user of this somehow
		}
		
		//We need to verify that the last character is a deliminator for directory traversal. This makes things simpler later.
		if (System.getProperty("os.name").startsWith("Windows") && shareDirectory.charAt(shareDirectory.length() - 1) != '\\') {
			shareDirectory += "\\"; //Need a backslash at the end for simplicity sake
		}
		else if (!System.getProperty("os.name").startsWith("Windows") && shareDirectory.charAt(shareDirectory.length() - 1) != '/') {
			shareDirectory += "/"; //Mac and Linux use / for directory traversal
		}
	}*/
	
	/**
	 * A function necessary to set the share directory inside the program itself. Editing the file will also work, but this
	 * creates a simpler way for users to set the share that is the target.
	 * @param shareDirectory
	 * @return True if the share is accessible, false if not. This is to check if the share is at least readable.
	 */
	public static boolean setShareDirectory(String share) {
		File f = new File(share);
		if (!f.exists()) return false; //This needs to be tested\
		String shareDirectory = "";
		synchronized (shareDirectory) {
			shareDirectory = share;
			/*if (System.getProperty("os.name").startsWith("Windows") && shareDirectory.charAt(shareDirectory.length() - 1) != '\\') {
				shareDirectory += "\\"; //Need a backslash at the end for simplicity sake
			}
			else if (!System.getProperty("os.name").startsWith("Windows") && shareDirectory.charAt(shareDirectory.length() - 1) != '/') {
				shareDirectory += "/"; //Mac and Linux use / for directory traversal
			}
			*/
			if(shareDirectory != "" && (shareDirectory.charAt(shareDirectory.length() - 1) != '/' || shareDirectory.charAt(shareDirectory.length() - 1) != '\\'))
			{
				shareDirectory += "/";
			}
		}
		
		Properties.setProperty(share, Properties.DIRECTORY);
		return true;
	}
	
	public static boolean exists(String filename)
	{
		String shareDirectory = Properties.getProperty(Properties.DIRECTORY);
		
		if(shareDirectory != "" && (shareDirectory.charAt(shareDirectory.length() - 1) != '/' || shareDirectory.charAt(shareDirectory.length() - 1) != '\\'))
		{
			shareDirectory += "/";
		}
		
		String fullName = shareDirectory + filename;
		
		return new File(fullName).exists();
	}
	
	public static void delete(String filename)
	{
		String shareDirectory = Properties.getProperty(Properties.DIRECTORY);
		
		if(shareDirectory != "" && (shareDirectory.charAt(shareDirectory.length() - 1) != '/' || shareDirectory.charAt(shareDirectory.length() - 1) != '\\'))
		{
			shareDirectory += "/";
		}
		
		String fullName = shareDirectory + filename;
		
		new File(fullName).delete();
	}
}
