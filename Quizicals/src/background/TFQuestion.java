package background;

import java.util.LinkedList;
import exceptions.InvalidTFQuestionException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class TFQuestion extends Question
{
	/**
	 * Constructor initializes fields.
	 * 
	 * @param questionString	Holds the question.
	 * @param answerStrings		Holds a list of possible answers.
	 * @param correctAnswer		Holds the correct answer.
	 * @param pointValue		Holds the amount of points the question is worth.
	 * @param section			Holds the section the question is from.
	 */
	public TFQuestion(String questionString, LinkedList<String> answerStrings, String correctAnswer, int pointValue, String section)
	{
		super(questionString, answerStrings, correctAnswer, pointValue, section);
		
		//check if the answers are actually true and false
		if (answerStrings.size() != 2)	//the answers are not true and false
		{
			throw new InvalidTFQuestionException();
		}
		else if ((!"true".equals(answerStrings.get(0).toLowerCase()) && !"t".equals(answerStrings.get(0).toLowerCase())) || 
				(!"false".equals(answerStrings.get(1).toLowerCase()) && !"f".equals(answerStrings.get(1).toLowerCase())))	//the answers are not true and false
		{
			throw new InvalidTFQuestionException();
		}	//end if
	}	//end of TFQuestion constructor
	
	/**
	 * Returns the list of possible answers.
	 * 
	 * @return	Returns the list of possible answers.
	 */
	public LinkedList<String> getRandomizedAnswers()
	{
		return this.answerStrings;
	}	//end of getRandomizedAnswers method
}	//end of TFQuestion class