package background;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import exceptions.NullSecurityException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public final class SecurityManager
{
	private static final String PASSWORD = "MzUI%-PwMrw&C$h+";
	private static final String IV = "aP3OpER+5J1K3:aE";
	
	/**
	 * Returns an encrypted byte representation 
	 * of the passed string.
	 * 
	 * @param info	Holds the string to be encrypted.
	 * @return	Returns an encrypted byte representation of the passed string.
	 */
	public static byte[] encrypt(byte[] info)
	{
		//check if info is null
		if (info == null)	//info is null
		{
			throw new NullSecurityException();
		}	//end if
		
		try
		{
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        SecretKeySpec key = new SecretKeySpec(PASSWORD.getBytes("UTF-8"), "AES");
	        IvParameterSpec iv = new IvParameterSpec(IV.getBytes("UTF-8"));
	        
	        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
	        
	        return cipher.doFinal(info);
		}
		catch (Exception e)
		{
			return null;
		}	//end try
	}	//end of encrypt method
	
	/**
	 * Returns a decrypted string from 
	 * a byte representation.
	 * 
	 * @param info	Holds the bytes to be decrypted. 
	 * @return	Returns a decrypted string from a byte representation.
	 */
	public static byte[] decrypt(byte[] info)
	{
		//check if info is null
		if (info == null)	//info is null
		{
			throw new NullSecurityException();
		}	//end if
		
		try
		{
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        SecretKeySpec key = new SecretKeySpec(PASSWORD.getBytes("UTF-8"), "AES");
	        IvParameterSpec iv = new IvParameterSpec(IV.getBytes("UTF-8"));
	        
	        cipher.init(Cipher.DECRYPT_MODE, key, iv);
	        
	        return cipher.doFinal(info);
		}
		catch (Exception e)
		{
			return null;
		}	//end try
	}	//end of decrypt method
}	//end of Security Manager class