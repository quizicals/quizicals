package background;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public final class Constants
{
	/**
	 * Holds the maximum amount of points a quiz can be worth.
	 */
	public static final int MAX_POINTS = 2000;
	
	/**
	 * Holds the maximum number of questions a quiz can have.
	 */
	public static final int MAX_QUESTIONS = 200;
	
	/**
	 * Holds the maximum number of answers a quiz can have.
	 */
	public static final int MAX_ANSWERS = 10;
	
	/**
	 * Holds the minimum number of characters in an account name.
	 */
	public static final int MIN_ACCOUNT_LENGTH = 6;
	
	/**
	 * Holds the maximum number of characters in an account name.
	 */
	public static final int MAX_ACCOUNT_LENGTH = 32;
	
	/**
	 * Holds the minimum number of characters in a password.
	 */
	public static final int MIN_PASSWORD_LENGTH  = 6;
	
	/**
	 * Holds the maximum number of characters in a password.
	 */
	public static final int MAX_PASSWORD_LENGTH = 32;
	
	/**
	 * Holds the title of the windows.
	 */
	public static final String TITLE = "Quizicals";
}	//end of Constants class