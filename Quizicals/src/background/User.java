package background;

public class User
{
	private UserType userType;
	private String firstName;
	private String lastName;
	private String username;
	private int userID;
	
	private static User current;
	
	public User(UserType userType, String firstName, String lastName, String username, int UID)
	{
		this.userType = userType;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.userID = UID;
	}
	
	public UserType getUserType()
	{
		return this.userType;
	}
	
	public String getFirstName()
	{
		return this.firstName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	
	public String getFullName()
	{
		return this.firstName + " " + this.lastName;
	}
	
	public String getUsername()
	{
		return this.username;
	}
	
	public int getUID()
	{
		return this.userID;
	}
	
	public static User getCurrentUser()
	{
		return User.current;
	}
	
	public static void setCurrentUser(User u)
	{
		User.current = u;
	}
	
	public static User setCurrentUser(UserType userType, String firstName, String lastName, String username, int UID)
	{
		User.current = new User(userType, firstName, lastName, username, UID);
		return User.current;
	}
}
