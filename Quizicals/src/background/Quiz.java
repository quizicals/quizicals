package background;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

import exceptions.InvalidQuestionsException;
import exceptions.InvalidQuizPointsException;
import exceptions.InvalidQuizQuestionsException;
import exceptions.NullFileNameException;
import exceptions.NullQuestionListException;
import exceptions.NullQuizInformationException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Quiz implements Serializable
{
	/**
	 * Holds a list of all the questions.
	 */
	private final LinkedList<Question> questions;
	
	/**
	 * Holds the name of a file.
	 */
	private final String fileName;
	
	/**
	 * Holds information about the quiz.
	 */
	private final QuizInformation quizInformation;
	
	protected Quiz(Quiz quiz, LinkedList<Question> questions)
	{
		this.questions = questions;
		this.fileName = quiz.fileName;
		this.quizInformation = quiz.quizInformation;
	}
	
	public Quiz(LinkedList<Question> questions, String fileName, QuizInformation quizInformation)
	{
		//check if questions is null
		if (questions == null)	//questions is null
		{
			throw new NullQuestionListException();
		}	//end if
		
		//check if the number of questions is valid
		if (questions.size() <= 0 || questions.size() > Constants.MAX_QUESTIONS)	//the number of questions is invalid
		{
			throw new InvalidQuestionsException(questions.size());
		}	//end if
		
		//check if the file name is null
		if (fileName == null)	//the file name is null
		{
			throw new NullFileNameException();
		}	//end if
		
		//check if the quiz information is null
		if (quizInformation == null)	//the quiz information is null
		{
			throw new NullQuizInformationException();
		}	//end if
		
		//check if the number of questions is equal to the number 
		//of questions in the quiz information
		if (questions.size() != quizInformation.getNumberOfQuestions())	//they are not equal
		{
			throw new InvalidQuizQuestionsException(questions.size(), quizInformation.getNumberOfQuestions());
		}	//end if
		
		//check if the total points in the quiz information 
		//equals the sum of the total points of each question
		int sum = 0;
		
		//loop through each question and sum
		for (Question q : questions)
		{
			sum += q.getPointValue();
		}	//end for
		
		//compare the sum to the quiz information total
		if (sum != quizInformation.getTotalPoints())	//they are not equal
		{
			throw new InvalidQuizPointsException(sum, quizInformation.getTotalPoints());
		}	//end if
		
		//set fields
		this.questions = questions;
		this.fileName = fileName;
		this.quizInformation = quizInformation;
	}	//end of Quiz constructor
	
	/**
	 * Returns a list of all the questions.
	 * 
	 * @return	Returns a list of all the questions.
	 */
	public LinkedList<Question> getQuestions()
	{
		return this.questions;
	}	//end of getQuestions method
	
	/**
	 * Returns a list of all the questions in a random order.
	 * 
	 * @return	Returns a list of all the questions in a random order.
	 */
	public LinkedList<Question> getRandomizedQuestions()
	{
		LinkedList<Question> randomList = this.questions;
		long seed = System.currentTimeMillis();
		
		Collections.shuffle(randomList, new Random(seed));
		
		return randomList;
	}	//end of getRandomizedQuestions method
	
	/**
	 * Returns the information about the quiz.
	 * 
	 * @return	Returns the information about the quiz.
	 */
	public QuizInformation getQuizInformation()
	{
		return this.quizInformation;
	}	//end of getQuizInformation method
	
	/**
	 * Returns the name of the file.
	 * 
	 * @return	Returns the name of the file.
	 */
	public String getFileName()
	{
		return this.fileName;
	}	//end of getFileName method
}	//end of Quiz class