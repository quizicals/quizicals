package background;

import java.util.ArrayList;
import exceptions.NullQuizScoresListException;
import exceptions.NullStudentNameException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Student
{
	/**
	 * Holds the name of the student.
	 */
	private final String name;
	
	/**
	 * Holds a list of the student's quiz scores.
	 */
	private final ArrayList<QuizScore> quizScores;
	
	/**
	 * Holds the student's ID.
	 */
	private final int studentID;
	
	/**
	 * Constructor initializes fields.
	 * 
	 * @param name			Holds the name of the student.
	 * @param quizScores	Holds a list of the student's quiz scores.
	 */
	public Student(int studentID, String name, ArrayList<QuizScore> quizScores)
	{
		//check if student's name is null
		if (name == null)
		{
			throw new NullStudentNameException();
		}	//end if
		
		//check if the quiz scores list is null
		if (quizScores == null)
		{
			throw new NullQuizScoresListException();
		}	//end if
		
		//set fields
		this.studentID = studentID;
		this.name = name;
		this.quizScores = quizScores;
	}	//end of Student constructor
	
	/**
	 * Returns the name of the student.
	 * 
	 * @return	Returns the name of the student.
	 */
	public String getName()
	{
		return this.name;
	}	//end of getName method
	
	/**
	 * Returns a list of the student's quiz scores.
	 * 
	 * @return	Returns a list of the student's quiz scores.
	 */
	public ArrayList<QuizScore> getQuizScores()
	{
		return this.quizScores;
	}	//end of getQuizScores method
	
	/**
	 * Returns the student's ID.
	 * 
	 * @return	Returns the student's ID.
	 */
	public int getStudentID()
	{
		return this.studentID;
	}
}	//end of Student class