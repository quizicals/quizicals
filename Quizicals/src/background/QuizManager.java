package background;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public final class QuizManager
{
	public static LinkedList<Question> questions = new LinkedList<Question>();
	public static QuizInformation quizInfo = null;
	public static int classID = -1;
	public static int quizID = -1;
	public static List<String> selectedAnswers;
	
	public static void clear()
	{
		questions.clear();
		quizInfo = null;
		classID = -1;
		quizID = -1;
		selectedAnswers = null;
	}
}	//end of QuizManager class