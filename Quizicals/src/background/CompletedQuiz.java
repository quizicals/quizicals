package background;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class CompletedQuiz extends Quiz implements Serializable
{
	private List<String> selectedAnswers;
	
	public CompletedQuiz(Quiz quiz, LinkedList<Question> questions)
	{
		super(quiz, questions);
		
		//questions on the quiz should match questions given
		if(quiz.getQuestions().size() != questions.size())
		{
			throw new IllegalArgumentException("quiz.getQuestions().size() (" + quiz.getQuestions().size() + ") != questions.size() (" + questions.size() + ")");
		}
		
		this.selectedAnswers = new LinkedList<String>();
	}
	
	public CompletedQuiz(Quiz quiz, LinkedList<Question> questions, List<String> selected)
	{
		super(quiz, questions);
		
		//questions on the quiz should match questions given
		if(quiz.getQuestions().size() != questions.size())
		{
			throw new IllegalArgumentException("quiz.getQuestions().size() (" + quiz.getQuestions().size() + ") != questions.size() (" + questions.size() + ")");
		}
		
		//there must be an answer to every question
		if(questions.size() != selected.size())
		{
			throw new IllegalArgumentException("selected.size() = " + selected.size() + " is not equal to questions.size() = " + this.getQuestions().size());
		}
		
		this.selectedAnswers = selected;
	}
	
	public void addSelectedAnswer(String answer)
	{
		this.selectedAnswers.add(answer);
	}
	
	public List<String> getSelectedAnswers()
	{
		return this.selectedAnswers;
	}
}
