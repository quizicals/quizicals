package background;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

import exceptions.InvalidAnswersException;
import exceptions.InvalidCorrectAnswerException;
import exceptions.InvalidPointsException;
import exceptions.NullAnswerStringsException;
import exceptions.NullCorrectAnswerException;
import exceptions.NullQuestionStringException;
import exceptions.NullSectionException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Question implements Serializable
{
	/**
	 * Holds the question.
	 */
	private final String questionString;
	
	/**
	 * Holds a list of possible answers.
	 */
	protected final LinkedList<String> answerStrings;
	
	/**
	 * Holds the correct answer.
	 */
	private final String correctAnswer;
	
	/**
	 * Holds the amount of points the question is worth.
	 */
	private final int pointValue;
	
	/**
	 * Holds the section the question is from.
	 */
	private final String section;
	
	/**
	 * Constructor initializes fields.
	 * 
	 * @param questionString	Holds the question.
	 * @param answerStrings		Holds a list of possible answers.
	 * @param correctAnswer		Holds the correct answer.
	 * @param pointValue		Holds the amount of points the question is worth.
	 * @param section			Holds the section the question is from.
	 */
	public Question(String questionString, LinkedList<String> answerStrings, String correctAnswer, int pointValue, String section)
	{
		//check if the question is null
		if (questionString == null)	//the question is null
		{
			throw new NullQuestionStringException();
		}	//end if
		
		//check if the answers are null
		if (answerStrings == null)	//the answers are null
		{
			throw new NullAnswerStringsException();
		}	//end if
		
		//check if the answers are valid
		if (answerStrings.size() <= 1 || answerStrings.size() > Constants.MAX_ANSWERS)	//the answers are invalid
		{
			throw new InvalidAnswersException(answerStrings);
		}	//end if
		
		//check if the correct answer is null
		if (correctAnswer == null)
		{
			throw new NullCorrectAnswerException();
		}	//end if
		
		//check if the correct answer is contained in the possible answers
		boolean isFound = false;
		
		for (String a : answerStrings)
		{
			//check if the answer is equal to one of the possible answers
			if (correctAnswer.toLowerCase().equals(a.toLowerCase()))	//the answer is equal
			{
				isFound = true;
				break;
			}	//end if
		}	//end for
		
		//check if the answer was found
		if (!isFound)	//the answer was not found
		{
			throw new InvalidCorrectAnswerException(correctAnswer);
		}	//end if
		
		//check if points are valid
		if (pointValue <= 0 || pointValue > Constants.MAX_POINTS)	//the points are invalid
		{
			throw new InvalidPointsException(pointValue);
		}	//end if
		
		//check if the section is null
		if (section == null)	//the section is null
		{
			throw new NullSectionException();
		}	//end if
		
		//set the fields
		this.questionString = questionString;
		this.answerStrings = answerStrings;
		this.correctAnswer = correctAnswer;
		this.pointValue = pointValue;
		this.section = section;
	}	//end of Question constructor
	
	/**
	 * Returns the question.
	 * 
	 * @return	Returns the question.
	 */
	public String getQuestionString()
	{
		return this.questionString;
	}	//end of getQuestionString method
	
	/**
	 * Returns a list of possible answers.
	 * 
	 * @return	Returns a list of possible answers.
	 */
	public LinkedList<String> getAnswers()
	{
		return this.answerStrings;
	}	//end of getAnswers method
	
	/**
	 * Returns the correct answer.
	 * 
	 * @return	Returns the correct answer.
	 */
	public String getCorrectAnswer()
	{
		return this.correctAnswer;
	}	//end of getCorrectAnswer method
	
	/**
	 * Returns if the answer is equal to the correct answer.
	 * True  = Equal
	 * False = Not Equal
	 * 
	 * @param answer	Holds the answer to test.
	 * @return			Returns if the answer is equal to the correct answer.
	 */
	public boolean isCorrect(String answer)
	{
		//check if answer is null
		if (answer == null)	//answer is null
		{
			return false;
		}	//end if
		
		return this.correctAnswer.toLowerCase().equals(answer.toLowerCase());
	}	//end of isCorrect method
	
	/**
	 * Returns the amount of points the question is worth.
	 * 
	 * @return	Returns the amount of points the question is worth.
	 */
	public int getPointValue()
	{
		return this.pointValue;
	}	//end of getPointValue method
	
	/**
	 * Returns the section the question is from.
	 * 
	 * @return	Returns the section the question is from.
	 */
	public String getSection()
	{
		return this.section;
	}	//end of getSection method
	
	/**
	 * Returns the list of possible answers in 
	 * a random order.
	 * 
	 * seed = currentTimeMillis
	 * 
	 * @return	Returns the list of possible answers in a random order.
	 */
	public LinkedList<String> getRandomizedAnswers()
	{
		LinkedList<String> randomList = this.answerStrings;
		long seed = System.currentTimeMillis();
		
		Collections.shuffle(randomList, new Random(seed));
		
		return randomList;
	}	//end of getRandomizedAnswers method
	
	public boolean equals(Question other)
	{
		if (!this.questionString.equals(other.questionString))
		{
			return false;
		}
		
		if (!this.answerStrings.equals(other.answerStrings))
		{
			return false;
		}
		
		if (!this.correctAnswer.equals(other.correctAnswer))
		{
			return false;
		}
		
		if (this.pointValue != other.pointValue)
		{
			return false;
		}
		
		if (!this.section.equals(other.section))
		{
			return false;
		}
		
		return true;
	}
}	//end of Question class