package background;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import exceptions.InvalidPropertiesException;
import exceptions.InvalidPropertyException;
import exceptions.NullPropertyException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public final class Properties
{
	public static final int DIRECTORY = 0;
	public static final int USERNAME = 1;
	public static final int PASSWORD = 2;
	private static final int NUM_PROPERTIES = 3;
	public static final String FILENAME = ".properties";
	public static final String DEFAULT_DIRECTORY = "";
	public static final String DEFAULT_USERNAME = "Quizicals";
	public static final String DEFAULT_PASSWORD = "BQeQtQmxAcDeVde3";

	/**
	 * Returns the properties as a list.
	 * 
	 * @return	Returns the properties as a list.
	 */
	public static ArrayList<String> getProperties()
	{
		ArrayList<String> properties = new ArrayList<String>();	//list of properties to return
		
		//initialize the properties list
		properties.add(DEFAULT_DIRECTORY);
		properties.add(DEFAULT_USERNAME);
		properties.add(DEFAULT_PASSWORD);
		
		File f = new File(FILENAME);
		
		byte[] encryptedContents = new byte[(int) f.length()]; //TODO fix for file > 4GB
		try {
			FileInputStream inputStream = new FileInputStream(f);
			inputStream.read(encryptedContents);
			inputStream.close();
		} 
		catch (IOException e) { 
			//The file cannot be read, most likely due to permissions
			//TODO what should I do in this case?
		}
		byte[] decryptedContents = SecurityManager.decrypt(encryptedContents);
		ByteArrayInputStream byteInputStream = new ByteArrayInputStream(decryptedContents);
		try {
			ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
			properties = (ArrayList<String>) objectInputStream.readObject();
			objectInputStream.close();
			
		}
		catch (ClassNotFoundException e) {
			//Abandon hope yee who arrive here
		}
		catch (IOException e) {
			
		}
		
		//try to read the properties from the file
		/*try
		{
			BufferedReader reader = new BufferedReader(new FileReader(FILENAME));	//file reader
			
			//read from the file and set properties
			for (int x = 0; x < NUM_PROPERTIES; x += 1)
			{
				String property = reader.readLine();	//read a line from the file
				
				//check if we are at the end of the file
				if (property == null)	//we are at the end of the file
				{
					break;
				}	//end if
				properties.set(x, property);
			}	//end for
			
			reader.close();
		}	
		catch (Exception exception)
		{
			
		}	//end try*/
		
		return properties;
	}	//end of getProperties method

	/**
	 * Returns the property at the given index.
	 * 
	 * @param index	Holds the index of the property.
	 * @return		Returns the property at the given index.
	 */
	public static String getProperty(int index)
	{
		//check if the index is out of bounds
		if (index < 0 || index >= NUM_PROPERTIES)	//the index is out of bounds
		{
			throw new InvalidPropertyException(index);
		}	//end if

		return getProperties().get(index);
	}	//end of getProperty method

	/**
	 * Sets all the properties.
	 * 
	 * @param properties	Holds the properties to be set.
	 */
	public static void setProperties(ArrayList<String> properties)
	{
		//check if the properties are null
		if (properties == null)	//the properties are null
		{
			throw new NullPropertyException();
		}	//end if

		//check if the number of properties is correct
		if (properties.size() != NUM_PROPERTIES)	//the number of properties is incorrect
		{
			throw new InvalidPropertiesException(properties.size(), NUM_PROPERTIES);
		}	//end if

		for (int x = 0; x < NUM_PROPERTIES; x += 1)
		{
			setProperty(properties.get(x), x);
		}	//end for
	}	//end of setProperties method

	/**
	 * Sets a specific property.
	 * 
	 * @param property	The value of the property.
	 * @param index		The index of the property.
	 */
	public static void setProperty(String property, int index)
	{
		//check if the property is null
		if (property == null)	//the property is null
		{
			throw new NullPropertyException();
		}	//end if

		//check if the index is out of bounds
		if (index < 0 || index >= NUM_PROPERTIES)	//the index is out of bounds
		{
			throw new InvalidPropertyException(index);
		}	//end if

		ArrayList<String> properties = Properties.getProperties();	//holds the properties list
		properties.set(index, property);	//set the new property
		
		//try to write to the file
		
		ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
		byte[] unencryptedContents;
		try {
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
			objectOutputStream.writeObject(properties);
			unencryptedContents = byteOutputStream.toByteArray();
			objectOutputStream.close();
		}
		catch (IOException e) {
			return; //TODO SOMETHING
		}
		byte[] encryptedContents = SecurityManager.encrypt(unencryptedContents);
		try {
			FileOutputStream fileOutput = new FileOutputStream(FILENAME);
			fileOutput.write(encryptedContents);
			fileOutput.close();
		}
		catch (IOException e) { 
			
		}
		
		/*try
		{
			PrintWriter writer = new PrintWriter(new FileWriter(FILENAME, false));	//file writer
			
			//write to the file
			for (int x = 0; x < NUM_PROPERTIES; x += 1)
			{
				writer.println(properties.get(x));
			}	//end for
			
			writer.close();
		}
		catch (Exception exception)
		{
			
		}	//end try*/
	}	//end of setProperty method
	
	public static void main(String[] args)
	{
		ArrayList<String> currentProperties = Properties.getProperties();
		int selection = Properties.selectProperty();
		
		while(selection != 3)
		{
			Scanner input = new Scanner(System.in);
			String value;
			
			System.out.println("Current property value = '" + currentProperties.get(selection) + "'");
			System.out.print("Enter new property value:  ");
			value = input.nextLine();
			currentProperties.set(selection, value);
			
			selection = Properties.selectProperty();
		}

		Properties.setProperties(currentProperties);
	}
	
	public static int selectProperty()
	{
		System.out.print(""
				+ "1. Change share directory\n"
				+ "2. Change database username\n"
				+ "3. Change database password\n"
				+ "4. Quit\n"
				+ "\n"
				+ "Enter number of the property to change:  ");
		Scanner input = new Scanner(System.in);
		int num = input.nextInt();
		
		while (num < 1 || num > 4)
		{
			System.out.print(""
					+ "1. Change share directory\n"
					+ "2. Change database username\n"
					+ "3. Change database password\n"
					+ "4. Quit\n"
					+ "\n"
					+ "Enter number of the property to change:  ");
			num = input.nextInt();
		}
		
		return num - 1;
	}
}	//end of Properties class