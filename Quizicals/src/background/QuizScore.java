package background;

import exceptions.InvalidPointsException;
import exceptions.NullQuizNameException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class QuizScore
{
	/**
	 * Holds the name of the quiz.
	 */
	private final String quizName;
	
	/**
	 * Holds the amount of points the quiz is worth.
	 */
	private final int points;
	
	/**
	 * Constructor initializes fields.
	 * 
	 * Throws an InvalidQuizException if an invalid points 
	 * value is passed.
	 * 
	 * @param quizName	Holds the name of the quiz.
	 * @param points	Holds the amount of points the quiz is worth.
	 */
	public QuizScore(String quizName, int points)
	{
		//check if the quiz name is null
		if (quizName == null)	//the quiz name is null
		{
			throw new NullQuizNameException();
		}	//end if
		
		//check if points are valid
		if (points < 0 || points > Constants.MAX_POINTS)
		{
			throw new InvalidPointsException(points);
		}	//end if
		
		//set fields
		this.quizName = quizName;
		this.points = points;
	}	//end of QuizScore constructor
	
	/**
	 * Returns the name of the quiz.
	 * 
	 * @return	Returns the name of the quiz.
	 */
	public String getQuizName()
	{
		return this.quizName;
	}	//end of getQuizName method
	
	/**
	 * Returns the how many points the quiz was worth.
	 * 
	 * @return	Returns the how many points the quiz was worth.
	 */
	public int getPoints()
	{
		return this.points;
	}	//end of getPoints method
}	//end of QuizScore class