package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Database
{
	public static void main(String[] args)
	{
		//declare variables
		String query;
		Scanner input = new Scanner(System.in);
		Database d = Database.getInstance();
		
		//ask for test data
		System.out.print("Would you like to insert test data into the database? (y/n):  ");
		query = input.nextLine();
		if(query.toLowerCase().equals("y"))
		{
			Database.insertTestValues();
			System.out.println("Test values added.");
		}
		else
		{
			System.out.println("Test values were not added.");
		}
		
		//get input
		System.out.print("Enter Query (quit to stop):  ");
		query = input.nextLine();
		
		//query until "quit" is entered
		while(!query.toLowerCase().equals("quit"))
		{
			//display results
			ResultSet rs = d.query(query);
			try
			{
				ResultSetMetaData rsmd = rs.getMetaData();
				
				while (rs.next())
				{
					for (int x = 1; x <= rsmd.getColumnCount(); x += 1)	
					{
						System.out.println(rsmd.getColumnName(x) + "=\"" + rs.getString(x) + "\"");
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			//get input
			System.out.print("Enter Query (quit to stop):  ");
			query = input.nextLine();
			
		}
		
		//cleanup
		d.close();
		input.close();
	}
	
	//instance of the database
	private static Database instance;
	private static int references = 0;
	
	private Connection con;
	
	private Database()
	{
		String url = "jdbc:mysql://localhost:3306/";
	    String user = "root";
	    String password = "";
	        
        try
        {
        	//connect
        	Class.forName("com.mysql.jdbc.Driver").newInstance();
            this.con = DriverManager.getConnection(url, user, password);
            
            //create statement to use
            Statement stt = con.createStatement();
            
            //create database if it does not exist and use database
            stt.execute("CREATE DATABASE IF NOT EXISTS quizicals");
            stt.execute("USE quizicals");
            
            //create tables for the database if they do not already exist
            stt.execute("CREATE TABLE IF NOT EXISTS classes ("
            		+ "classID BIGINT NOT NULL AUTO_INCREMENT,"
            		+ "className VARCHAR(20),"
            		+ "teacherID BIGINT,"
            		+ "PRIMARY KEY (classID)"
            		+ ")");
            stt.execute("CREATE TABLE IF NOT EXISTS enrollment ("
            		+ "enrollmentID BIGINT NOT NULL AUTO_INCREMENT,"
            		+ "studentID BIGINT,"
            		+ "classID BIGINT,"
            		+ "PRIMARY KEY (enrollmentID)"
            		+ ")");
            stt.execute("CREATE TABLE IF NOT EXISTS quizzes ("
            		+ "quizID BIGINT NOT NULL AUTO_INCREMENT,"
            		+ "quizName VARCHAR(32),"
            		+ "classID BIGINT,"
            		+ "PRIMARY KEY (quizID)"
            		+ ")");
            stt.execute("CREATE TABLE IF NOT EXISTS scores ("
            		+ "scoreID BIGINT NOT NULL AUTO_INCREMENT,"
            		+ "quizID BIGINT,"
            		+ "enrollmentID BIGINT,"
            		+ "studentID BIGINT,"
            		+ "score INT(11),"
            		+ "PRIMARY KEY (scoreID)"
            		+ ")");
            stt.execute("CREATE TABLE IF NOT EXISTS students ("
            		+ "studentID BIGINT NOT NULL AUTO_INCREMENT,"
            		+ "firstName VARCHAR(25),"
            		+ "lastName VARCHAR(25),"
            		+ "username VARCHAR(25),"
            		+ "password VARCHAR(250),"
            		+ "securityQuestion VARCHAR(140),"
            		+ "securityAnswer VARCHAR(32),"
            		+ "PRIMARY KEY (studentID),"
            		+ "UNIQUE (username)"
            		+ ")");
            stt.execute("CREATE TABLE IF NOT EXISTS teachers ("
            		+ "teacherID BIGINT NOT NULL AUTO_INCREMENT,"
            		+ "firstName VARCHAR(25),"
            		+ "lastName VARCHAR(25),"
            		+ "username VARCHAR(25),"
            		+ "password VARCHAR(250),"
            		+ "securityQuestion VARCHAR(140),"
            		+ "securityAnswer VARCHAR(32),"
            		+ "PRIMARY KEY (teacherID),"
            		+ "UNIQUE (username)"
            		+ ")");
            
            //close statement
            stt.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
	}
	
	public static void insertTestValues()
	{
		Database db = Database.getInstance();
		
		try
		{
			Statement stt = db.con.createStatement();
			
			//add some test data
	        ResultSet rs = stt.executeQuery("SELECT * FROM teachers WHERE firstName='Bob' AND lastName='Ross' AND username='bross1'");
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO teachers (firstName, lastName, username, password, securityQuestion, securityAnswer) VALUES ('Bob', 'Ross', 'bross1', 'password', 'Is that a happy little tree?', 'yes')");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM teachers WHERE firstName='John' AND lastName='Cena' AND username='jcena1'");
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO teachers (firstName, lastName, username, password, securityQuestion, securityAnswer) VALUES ('John', 'Cena', 'jcena1', 'password', 'WHO IS IT??', 'JOHN CENA')");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM students WHERE firstName='Mike' AND lastName='Wazowski' AND username='mwazowski'");
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO students (firstName, lastName, username, password, securityQuestion, securityAnswer) VALUES ('Mike', 'Wazowski', 'mwazowski', 'password', 'Ch', 'boy')");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM students WHERE firstName='Ash' AND lastName='Ketchum' AND username='aketchum'");
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO students (firstName, lastName, username, password, securityQuestion, securityAnswer) VALUES ('Ash', 'Ketchum', 'aketchum', 'password', 'Gotta catch em all', 'pokemon')");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT teacherID FROM teachers WHERE username='jcena1'");
	        rs.next();
	        int cena = rs.getInt("teacherID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM classes WHERE className='WWE' AND teacherID=" + cena);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO classes (className, teacherID) VALUES ('WWE', " + cena + ")");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM classes WHERE className='You can\\'t see me' AND teacherID=" + cena);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO classes (className, teacherID) VALUES ('You can\\'t see me', " + cena + ")");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT teacherID FROM teachers WHERE username='bross1'");
	        rs.next();
	        int ross = rs.getInt("teacherID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM classes WHERE className='The Joy of Painting' AND teacherID=" + ross);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO classes (className, teacherID) VALUES ('The Joy of Painting', " + ross + ")");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM classes WHERE className='Happy Little Trees' AND teacherID=" + ross);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO classes (className, teacherID) VALUES ('Happy Little Trees', " + ross + ")");
	        }
	        rs.close();
	
	        rs = stt.executeQuery("SELECT studentID FROM students WHERE username='mwazowski'");
	        rs.next();
	        int mike = rs.getInt("studentID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT studentID FROM students WHERE username='aketchum'");
	        rs.next();
	        int ash = rs.getInt("studentID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT classID FROM classes WHERE className='WWE'");
	        rs.next();
	        int wwe = rs.getInt("classID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT classID FROM classes WHERE className='You can\\'t see me'");
	        rs.next();
	        int ycsm = rs.getInt("classID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT classID FROM classes WHERE className='The Joy of Painting'");
	        rs.next();
	        int joy = rs.getInt("classID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT classID FROM classes WHERE className='Happy Little Trees'");
	        rs.next();
	        int happy = rs.getInt("classID");
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM enrollment WHERE studentID=" + ash + " AND classID=" + wwe);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO enrollment (studentID, classID) VALUES (" + ash + ", " + wwe + ")");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM enrollment WHERE studentID=" + mike + " AND classID=" + ycsm);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO enrollment (studentID, classID) VALUES (" + mike + ", " + ycsm + ")");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM enrollment WHERE studentID=" + ash + " AND classID=" + joy);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO enrollment (studentID, classID) VALUES (" + ash + ", " + joy + ")");
	        }
	        rs.close();
	        
	        rs = stt.executeQuery("SELECT * FROM enrollment WHERE studentID=" + mike + " AND classID=" + happy);
	        if(!rs.next())
	        {
	        	stt.execute("INSERT INTO enrollment (studentID, classID) VALUES (" + mike + ", " + happy + ")");
	        }
	        rs.close();
	        
	        stt.close();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
			return;
		}
	}
	
	public static Database getInstance()
	{
		//increment number of references
		Database.references++;
		
		//return instance of database
		if(Database.instance == null)
		{
			Database.instance = new Database();
		}
		return Database.instance;
	}
	
	public ResultSet query(String query)
	{
		try
		{
			 Statement statement = con.createStatement();
			 
			 return statement.executeQuery(query);
		}
		catch(SQLException ex)
		{
			try
			{
				Statement statement = con.createStatement();
				statement.execute(query);
			}
			catch(SQLException ex2)
			{
				System.err.println(ex2.getMessage());
			}
			
			return null;
		}
	}
	
	public void close()
	{
		Database.references--;
		if(Database.references == 0)
		{
			try
			{
				this.con.close();
				Database.instance = null;
			}
			catch(SQLException ex)
			{
				System.err.println(ex.getMessage());
			}
		}
	}
}