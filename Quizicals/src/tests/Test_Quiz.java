package tests;

import java.util.LinkedList;
import background.Constants;
import background.Question;
import background.Quiz;
import background.QuizInformation;
import exceptions.InvalidQuestionsException;
import exceptions.InvalidQuizPointsException;
import exceptions.InvalidQuizQuestionsException;
import exceptions.NullFileNameException;
import exceptions.NullQuestionListException;
import exceptions.NullQuizInformationException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_Quiz
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 10;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_Quiz");
		
		int points = 0;
		
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		points += test5();
		points += test6();
		points += test7();
		points += test8();
		points += test9();
		points += test10();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x3
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 30, 3)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 4;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		questions.add(new Question("Q1", answers, "1", 10, "Section"));
		questions.add(new Question("Q2", answers, "1", 10, "Section"));
		questions.add(new Question("Q3", answers, "1", 10, "Section"));
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 30, 3);
		Quiz q = new Quiz(questions, fileName, quizInformation);
		
		//check if the question lists are equal
		if (questions.equals(q.getQuestions()))	//the question lists are equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the question lists are not equal
		{
			System.out.println("Failure:  Question lists are not equal.");
		}	//end if
		
		//check if the file names are equal
		if (fileName.equals(q.getFileName()))	//the file names are equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the file names are not equal
		{
			System.out.println("Failure:  \"" + fileName + "\" != \"" + q.getFileName() + "\"");
		}	//end if
		
		//check if the quiz informations are equal
		if (quizInformation.equals(q.getQuizInformation()))	//the quiz informations are equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the quiz informations are not equal
		{
			System.out.println("Failure:  Quiz informations are not equal.");
		}	//end if
		
		//check if the questions are in the same order
		boolean isFound = false;
				
		//check if the lists are the same size
		if (questions.size() == q.getRandomizedQuestions().size())	//the lists are the same size
		{
			//loop to make sure they don't just random to the same order
			for (int x = 0; x < 1000 && !isFound; x += 1)
			{
				//loop through all the questions
				for (int y = 0; y < questions.size(); y += 1)
				{
					//check if the question is equal
					if (!questions.get(y).equals(q.getRandomizedQuestions().get(y)))	//the questions are not in the same order
					{
						isFound = true;
						break;
					}	//end if
				}	//end for
			}	//end for
		}	//end if
				
		//check if we found a difference
		if (isFound)	//we found a difference
		{
			System.out.println("Success");
			points += 1;
		}
		else	//we did not find a difference
		{
			System.out.println("Failure:  Question list did not randomize.");
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x0
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 30, 3)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 30, 3);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  InvalidQuestionsException");
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = null
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 30, 3)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = null;
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 30, 3);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  NullQuestionListException");
		}
		catch (NullQuestionListException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x1
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 10, 1)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		questions.add(new Question("Q1", answers, "1", 10, "Section"));
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 10, 1);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Success");
			points += 1;
		}
		catch (NullQuestionListException e)
		{
			System.out.println("Failure:  NullQuestionListException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = MAX_QUESTIONS
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", MAX_POINTS, MAX_QUESTIONS)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test5()
	{
		System.out.println("test5");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		
		//add questions to the list
		for (int x = 1; x <= Constants.MAX_QUESTIONS; x += 1)
		{
			questions.add(new Question("Q" + x, answers, "1", Constants.MAX_POINTS / Constants.MAX_QUESTIONS, "Section"));
		}	//end for
		
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", Constants.MAX_POINTS, Constants.MAX_QUESTIONS);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Failure:  InvalidQuestionsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test5 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = MAX_QUESTIONS + 1
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 30, 10)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test6()
	{
		System.out.println("test6");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		
		//add questions to the list
		for (int x = 1; x <= Constants.MAX_QUESTIONS + 1; x += 1)
		{
			questions.add(new Question("Q" + x, answers, "1", Constants.MAX_POINTS / (Constants.MAX_QUESTIONS + 1), "Section"));
		}	//end for
		
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", Constants.MAX_POINTS, Constants.MAX_QUESTIONS);
			
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  InvalidQuestionsException");
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test6 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x3
	 * fileName = null
	 * quizInformation = ("Quiz", "Teacher", 30, 3)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test7()
	{
		System.out.println("test7");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		questions.add(new Question("Q1", answers, "1", 10, "Section"));
		questions.add(new Question("Q2", answers, "1", 10, "Section"));
		questions.add(new Question("Q3", answers, "1", 10, "Section"));
		String fileName = null;
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 30, 3);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  NullFileNameException");
		}
		catch (NullFileNameException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test7 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x3
	 * fileName = "File"
	 * quizInformation = null
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test8()
	{
		System.out.println("test8");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		questions.add(new Question("Q1", answers, "1", 10, "Section"));
		questions.add(new Question("Q2", answers, "1", 10, "Section"));
		questions.add(new Question("Q3", answers, "1", 10, "Section"));
		String fileName = "File";
		QuizInformation quizInformation = null;
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  NullQuizInformationException");
		}
		catch (NullQuizInformationException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test8 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x2
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 30, 3)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test9()
	{
		System.out.println("test9");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		questions.add(new Question("Q1", answers, "1", 10, "Section"));
		questions.add(new Question("Q2", answers, "1", 10, "Section"));
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 30, 3);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  InvalidQuizQuestionsException");
		}
		catch (InvalidQuizQuestionsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test9 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questions = x3
	 * fileName = "File"
	 * quizInformation = ("Quiz", "Teacher", 30, 3)
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test10()
	{
		System.out.println("test10");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		LinkedList<Question> questions = new LinkedList<Question>();
		LinkedList<String> answers = new LinkedList<String>();
		answers.add("1");
		answers.add("2");
		answers.add("3");
		questions.add(new Question("Q1", answers, "1", 10, "Section"));
		questions.add(new Question("Q2", answers, "1", 10, "Section"));
		questions.add(new Question("Q3", answers, "1", 10, "Section"));
		String fileName = "File";
		QuizInformation quizInformation = new QuizInformation("Quiz", "Teacher", 25, 3);
		
		try
		{
			new Quiz(questions, fileName, quizInformation);
			System.out.println("Failure:  InvalidQuizPointsException");
		}
		catch (InvalidQuizPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test10 method
}	//end of Test_Quiz class