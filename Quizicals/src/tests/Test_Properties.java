package tests;

import java.io.File;
import java.util.ArrayList;
import background.Properties;
import exceptions.InvalidPropertyException;
import exceptions.NullPropertyException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_Properties
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 6;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_Question");
		
		int points = 0;
		
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		points += test5();
		points += test6();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * Set and read properties.
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 11;
		int points = 0;
		
		ArrayList<String> properties = new ArrayList<String>();
		properties.add("Directory");
		properties.add("Username");
		properties.add("Password");
		
		//try to set properties
		try
		{
			Properties.setProperties(properties);
			System.out.println("Success");
			points += 1;
		}
		catch (Exception e)
		{
			System.out.print("Failure:  ");
			
			if (e instanceof NullPropertyException)
			{
				System.out.println("NullPropertyException");
			}
			else
			{
				System.out.println("InvalidPropertiesException");
			}
		}	//end try
		
		//try to get properties
		try
		{
			ArrayList<String> p = Properties.getProperties();
			
			//check if the properties are what we expect
			if (properties.equals(p))	//the properties are what we expect
			{
				System.out.println("Success");
				points += 1;
			}
			else	//the properties are not what we expect
			{
				System.out.print("Failure:  ");
				
				for (int x = 0; x < 3; x += 1)
				{
					System.out.print("\"" + properties.get(x) + "\"" + "!=" + "\"" + p.get(x) + "\" ");
				}	//end for
				System.out.println();
			}	//end if
		}
		catch (Exception e)
		{
			System.out.println("Failure");
		}	//end try
		
		//try to get the properties one at a time
		for (int x = 0; x < 3; x += 1)
		{
			try
			{
				String property = Properties.getProperty(x);
				
				//check if the property is what we expect
				if (properties.get(x).equals(property))	//the property is what we expect
				{
					System.out.println("Success");
					points += 1;
				}
				else	//the property is not what we expect
				{
					System.out.println("Failure:  \"" + property + "\"!=\"" + properties.get(x) + "\"");
				}	//end if
			}
			catch (InvalidPropertyException e)
			{
				System.out.println("Failure:  index=" + x);
			}	//end try
		}	//end for
		
		//try to set each property one at a time
		for (int x = 0; x < 3; x += 1)
		{
			try
			{
				properties.set(x, properties.get(x) + "1");
				Properties.setProperty(properties.get(x), x);
				String property = Properties.getProperty(x);
				
				//check if the property was properly set
				if (properties.get(x).equals(property))	//the property was properly set
				{
					System.out.println("Success");
					points += 1;
				}
				else	//the property was not properly set
				{
					System.out.println("Failure:  \"" + property + "\"!=\"" + properties.get(x) + "\"");
				}	//end if
				
				//check if getting all the properties works fine
				ArrayList<String> p = Properties.getProperties();
				if (properties.equals(p))	//getting all the properties works fine
				{
					System.out.println("Success");
					points += 1;
				}
				else	//getting all the properties does not work fine
				{
					System.out.print("Failure:  ");
					
					for (int y = 0; y < 3; y += 1)
					{
						System.out.print("\"" + properties.get(y) + "\"" + "!=" + "\"" + p.get(y) + "\" ");
					}	//end for
					System.out.println();
				}	//end if
			}
			catch (Exception e)
			{
				System.out.print("Failure:  ");
				if (e instanceof NullPropertyException)
				{
					System.out.println("NullPropertyException");
				}
				else
				{
					System.out.println("InvalidPropertiesException");
				}
			}	//end try
		}	//end for
			
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * Try getting properties when there is no file.
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 4;
		int points = 0;
		
		new File(Properties.FILENAME).delete();
		
		ArrayList<String> properties = new ArrayList<String>();
		properties.add(Properties.DEFAULT_DIRECTORY);
		properties.add(Properties.DEFAULT_USERNAME);
		properties.add(Properties.DEFAULT_PASSWORD);
		
		try
		{
			ArrayList<String> p = Properties.getProperties();
			
			//check if the properties are equal
			if (properties.equals(p))	//check if the properties are equal
			{
				System.out.println("Success");
				points += 1;
			}
			else	//check if the properties are not equal
			{
				System.out.print("Failure:  ");
				
				for (int y = 0; y < 3; y += 1)
				{
					System.out.print("\"" + properties.get(y) + "\"" + "!=" + "\"" + p.get(y) + "\" ");
				}	//end for
				System.out.println();
			}	//end if
		}
		catch (Exception e)
		{
			System.out.println("Failure");
		}
		
		//check to make sure that getting one property works fine
		for (int x = 0; x < 3; x += 1)
		{
			try
			{
				String property = Properties.getProperty(x);
				
				if (properties.get(x).equals(property))
				{
					System.out.println("Success");
					points += 1;
				}
				else
				{
					System.out.println("Failure:  \"" + property + "\"!=\"" + properties.get(x) + "\"");
				}	//end if
			}
			catch (Exception e)
			{
				System.out.println("Failure");
			}	//end try
		}	//end for
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * Try setting a non-zero property with no file.
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		new File(Properties.FILENAME).delete();
		
		Properties.setProperty("user", Properties.USERNAME);
		String property = Properties.getProperty(Properties.USERNAME);
		
		if ("user".equals(property))
		{
			System.out.println("Success");
			points += 1;
		}
		else
		{
			System.out.println("Failure:  \"" + property + "\"!=\"user\"");
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * Try different indexes with getProperty.
	 * 
	 * index = -1
	 * index = 0
	 * index = 1
	 * index = 2
	 * index = 3
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 5;
		int points = 0;
		
		try
		{
			Properties.getProperty(-1);
			System.out.println("Failure:  index=-1");
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		try
		{
			Properties.getProperty(0);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Failure:  index=0");
		}	//end try
		
		try
		{
			Properties.getProperty(1);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Failure:  index=1");
		}	//end try
		
		try
		{
			Properties.getProperty(2);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Failure:  index=2");
		}	//end try
		
		try
		{
			Properties.getProperty(3);
			System.out.println("Failure:  index=3");
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * Try different indexes with setProperty.
	 * 
	 * index = -1
	 * index = 0
	 * index = 1
	 * index = 2
	 * index = 3
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test5()
	{
		System.out.println("test5");
		System.out.println("------------");
		
		int maxPoints = 5;
		int points = 0;
		
		try
		{
			Properties.setProperty("test", -1);
			System.out.println("Failure:  index=-1");
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		try
		{
			Properties.setProperty("test", 0);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Failure:  index=0");
		}	//end try
		
		try
		{
			Properties.setProperty("test", 1);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Failure:  index=1");
		}	//end try
		
		try
		{
			Properties.setProperty("test", 2);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Failure:  index=2");
		}	//end try
		
		try
		{
			Properties.setProperty("test", 3);
			System.out.println("Failure:  index=3");
		}
		catch (InvalidPropertyException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test5 method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * Try setting null properties
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test6()
	{
		System.out.println("test6");
		System.out.println("------------");
		
		int maxPoints = 2;
		int points = 0;
		
		try
		{
			Properties.setProperties(null);
			System.out.println("Failure:  NullPropertyException");
		}
		catch (NullPropertyException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		try
		{
			Properties.setProperty(null, 0);
			System.out.println("Failure:  NullPropertyException");
		}
		catch (NullPropertyException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test6 method
}	//end of Test_Properties class