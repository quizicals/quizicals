package tests;

import java.util.LinkedList;
import background.Constants;
import background.Question;
import exceptions.InvalidAnswersException;
import exceptions.InvalidCorrectAnswerException;
import exceptions.InvalidPointsException;
import exceptions.NullAnswerStringsException;
import exceptions.NullCorrectAnswerException;
import exceptions.NullQuestionStringException;
import exceptions.NullSectionException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_Question
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 18;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_Question");
		
		int points = 0;
		
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		points += test5();
		points += test6();
		points += test7();
		points += test8();
		points += test9();
		points += test10();
		points += test11();
		points += test12();
		points += test13();
		points += test14();
		points += test15();
		points += test16();
		points += test17();
		points += test18();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 7;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		Question q = new Question(questionString, answerStrings, correctAnswer, pointValue, section);
		
		//check if the question is equal
		if (questionString.equals(q.getQuestionString()))	//the question is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the question is not equal
		{
			System.out.println("Failure:  \"" + questionString + "\" != \"" + q.getQuestionString() + "\"");
		}	//end if
		
		//check if the answers are equal
		if (answerStrings.equals(q.getAnswers()))	//the answers are equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the answers are not equal
		{
			System.out.println("Failure:  Answers are not equal.");
		}	//end if
		
		//check if the correct answer is equal
		if (correctAnswer.equals(q.getCorrectAnswer()))	//the correct answer is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the correct answer is not equal
		{
			System.out.println("Failure:  " + correctAnswer + " != " + q.getCorrectAnswer());
		}	//end if
		
		//check if the correct answer is equal
		if (q.isCorrect(correctAnswer))	//the correct answer is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the correct answer is not equal
		{
			System.out.println("Failure:  \"" + correctAnswer + "\" != \"" + q.getCorrectAnswer() + "\"");
		}	//end if
		
		//check if the point value is equal
		if (pointValue == q.getPointValue())	//the point value is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the point value is not equal
		{
			System.out.println("Failure:  " + pointValue + " != " + q.getPointValue());
		}	//end if
		
		//check if the section is equal
		if (section.equals(q.getSection()))	//the section is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the section is not equal
		{
			System.out.println("Failure:  \"" + section + "\" != \"" + q.getSection() + "\"");
		}	//end if
		
		//check if the answers are in the same order
		boolean isFound = false;
		
		//check if the lists are the same size
		if (answerStrings.size() == q.getRandomizedAnswers().size())	//the lists are the same size
		{
			//loop to make sure they don't just random to the same order
			for (int x = 0; x < 1000 && !isFound; x += 1)
			{
				//loop through all the answers
				for (int y = 0; y < answerStrings.size(); y += 1)
				{
					//check if the answer is equal
					if (!answerStrings.get(y).equals(q.getRandomizedAnswers().get(y)))	//the answers are not in the same order
					{
						isFound = true;
						break;
					}	//end if
				}	//end for
			}	//end for
		}	//end if
		
		//check if we found a difference
		if (isFound)	//we found a difference
		{
			System.out.println("Success");
			points += 1;
		}
		else	//we did not find a difference
		{
			System.out.println("Failure:  Answers list did not randomize.");
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = null
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = null;
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  NullQuestionStringException");
		}
		catch (NullQuestionStringException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = null
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = null;
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  NullAnswerStringsException");
		}
		catch (NullAnswerStringsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidAnswersException");
		}
		catch (InvalidAnswersException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1"}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test5()
	{
		System.out.println("test5");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidAnswersException");
		}
		catch (InvalidAnswersException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test5 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2"}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test6()
	{
		System.out.println("test6");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidAnswersException e)
		{
			System.out.println("Failure:  InvalidAnswersException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test6 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = MAX_ANSWERS
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test7()
	{
		System.out.println("test7");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		
		//add to the answerStrings list
		for (int x = 1; x <= Constants.MAX_ANSWERS; x += 1)
		{
			answerStrings.add("" + x);
		}	//end for
		
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidAnswersException e)
		{
			System.out.println("Failure:  InvalidAnswersException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test7 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = MAX_ANSWERS + 1
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test8()
	{
		System.out.println("test8");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		
		//add to the answerStrings list
		for (int x = 1; x <= Constants.MAX_ANSWERS + 1; x += 1)
		{
			answerStrings.add("" + x);
		}	//end for
		
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidAnswersException");
		}
		catch (InvalidAnswersException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test8 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = null
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test9()
	{
		System.out.println("test9");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = null;
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  NullCorrectAnswerException");
		}
		catch (NullCorrectAnswerException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test9 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "4"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test10()
	{
		System.out.println("test10");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "4";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidCorrectAnswerException");
		}
		catch (InvalidCorrectAnswerException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test10 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = -1
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test11()
	{
		System.out.println("test11");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = -1;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidPointsException");
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test11 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = 0
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test12()
	{
		System.out.println("test12");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = 0;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidPointsException");
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test12 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = 1
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test13()
	{
		System.out.println("test13");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = 1;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidPointsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test13 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = MAX_POINTS
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test14()
	{
		System.out.println("test14");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = Constants.MAX_POINTS;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidPointsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test14 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = MAX_POINTS + 1
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test15()
	{
		System.out.println("test15");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = Constants.MAX_POINTS + 1;
		String section = "Section";
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidPointsException");
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test15 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = null
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test16()
	{
		System.out.println("test16");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = 10;
		String section = null;
		
		try
		{
			new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  NullSectionException");
		}
		catch (NullSectionException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test16 method
	
	/**
	 * Runs a regression test case (ID=2) and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"1", "2", "3"}
	 * correctAnswer = "1"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test17()
	{
		System.out.println("test17");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("1");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "1";
		int pointValue = 10;
		String section = "Section";
		
		Question q = new Question(questionString, answerStrings, correctAnswer, pointValue, section);
		
		//check if the answer is correct 
		if (!q.isCorrect("asdf"))	//check if the answer is not correct
		{
			System.out.println("Success");
			points += 1;
		}
		else	//check if the answer is not correct
		{
			System.out.println("Failure:  isCorrect");
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test17 method
	
	/**
	 * Runs a regression test case (ID=3) and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"A", "2", "3"}
	 * correctAnswer = "a"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test18()
	{
		System.out.println("test18");
		System.out.println("------------");
		
		int maxPoints = 2;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("A");
		answerStrings.add("2");
		answerStrings.add("3");
		String correctAnswer = "a";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			Question q = new Question(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
			
			//check if the answer is correct
			if (q.isCorrect("A"))	//the answer is correct
			{
				System.out.println("Success");
				points += 1;
			}
			else	//the answer is not correct
			{
				System.out.println("Failure:  \"A\" != \"" + q.getCorrectAnswer() + "\"");
			}	//end if
		}
		catch (InvalidCorrectAnswerException e)
		{
			System.out.println("Failure:  InvalidCorrectAnswerException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test18 method
}	//end of Test_Question class