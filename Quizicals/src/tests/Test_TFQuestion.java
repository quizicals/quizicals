package tests;

import java.util.LinkedList;
import background.TFQuestion;
import exceptions.InvalidCorrectAnswerException;
import exceptions.InvalidTFQuestionException;

public class Test_TFQuestion
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 11;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_TFQuestion");
		
		int points = 0;
		
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		points += test5();
		points += test6();
		points += test7();
		points += test8();
		points += test9();
		points += test10();
		points += test11();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"True", "False"}
	 * correctAnswer = "True"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 7;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("True");
		answerStrings.add("False");
		String correctAnswer = "True";
		int pointValue = 10;
		String section = "Section";
		
		TFQuestion q = new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
		
		//check if the question is equal
		if (questionString.equals(q.getQuestionString()))	//the question is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the question is not equal
		{
			System.out.println("Failure:  \"" + questionString + "\" != \"" + q.getQuestionString() + "\"");
		}	//end if
		
		//check if the answers are equal
		if (answerStrings.equals(q.getAnswers()))	//the answers are equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the answers are not equal
		{
			System.out.println("Failure:  Answers are not equal.");
		}	//end if
		
		//check if the correct answer is equal
		if (correctAnswer.equals(q.getCorrectAnswer()))	//the correct answer is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the correct answer is not equal
		{
			System.out.println("Failure:  " + correctAnswer + " != " + q.getCorrectAnswer());
		}	//end if
		
		//check if the correct answer is equal
		if (q.isCorrect(correctAnswer))	//the correct answer is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the correct answer is not equal
		{
			System.out.println("Failure:  \"" + correctAnswer + "\" != \"" + q.getCorrectAnswer() + "\"");
		}	//end if
		
		//check if the point value is equal
		if (pointValue == q.getPointValue())	//the point value is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the point value is not equal
		{
			System.out.println("Failure:  " + pointValue + " != " + q.getPointValue());
		}	//end if
		
		//check if the section is equal
		if (section.equals(q.getSection()))	//the section is equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the section is not equal
		{
			System.out.println("Failure:  \"" + section + "\" != \"" + q.getSection() + "\"");
		}	//end if
		
		//check if the answers are in the same order
		boolean isFound = false;
		
		//check if the lists are the same size
		if (answerStrings.size() == q.getRandomizedAnswers().size())	//the lists are the same size
		{
			//loop to make sure they don't just random to the same order
			for (int x = 0; x < 1000 && !isFound; x += 1)
			{
				//loop through all the answers
				for (int y = 0; y < answerStrings.size(); y += 1)
				{
					//check if the answer is equal
					if (!answerStrings.get(y).equals(q.getRandomizedAnswers().get(y)))	//the answers are not in the same order
					{
						isFound = true;
						break;
					}	//end if
				}	//end for
			}	//end for
		}	//end if
		
		//check if we found a difference
		if (isFound)	//we found a difference
		{
			System.out.println("Failure:  Answers list randomized.");
		}
		else	//we did not find a difference
		{
			System.out.println("Success");
			points += 1;
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"True", "True"}
	 * correctAnswer = "True"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("True");
		answerStrings.add("True");
		String correctAnswer = "True";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidTFQuestionException");
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"False", "False"}
	 * correctAnswer = "False"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("False");
		answerStrings.add("False");
		String correctAnswer = "False";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidTFQuestionException");
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"False", "True"}
	 * correctAnswer = "False"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("False");
		answerStrings.add("True");
		String correctAnswer = "False";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidTFQuestionException");
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"True", "asdf"}
	 * correctAnswer = "True"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test5()
	{
		System.out.println("test5");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("True");
		answerStrings.add("asdf");
		String correctAnswer = "True";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidTFQuestionException");
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test5 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"TRUE", "FALSE"}
	 * correctAnswer = "True"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test6()
	{
		System.out.println("test6");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("TRUE");
		answerStrings.add("FALSE");
		String correctAnswer = "True";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Failure:  InvalidTFQuestionException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test6 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"t", "f"}
	 * correctAnswer = "t"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test7()
	{
		System.out.println("test7");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("t");
		answerStrings.add("f");
		String correctAnswer = "t";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Failure:  InvalidTFQuestionException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test7 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"t", "F"}
	 * correctAnswer = "t"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test8()
	{
		System.out.println("test8");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("t");
		answerStrings.add("F");
		String correctAnswer = "t";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Failure:  InvalidTFQuestionException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test8 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"T", "F"}
	 * correctAnswer = "t"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test9()
	{
		System.out.println("test9");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("T");
		answerStrings.add("F");
		String correctAnswer = "t";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Failure:  InvalidTFQuestionException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test9 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"t", "f"}
	 * correctAnswer = "true"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test10()
	{
		System.out.println("test10");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("t");
		answerStrings.add("f");
		String correctAnswer = "true";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidCorrectAnswerException");
		}
		catch (InvalidCorrectAnswerException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test10 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * questionString = "Question"
	 * answerStrings = {"f", "t"}
	 * correctAnswer = "t"
	 * pointValue = 10
	 * section = "Section"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test11()
	{
		System.out.println("test11");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		String questionString = "Question";
		LinkedList<String> answerStrings = new LinkedList<String>();
		answerStrings.add("f");
		answerStrings.add("t");
		String correctAnswer = "t";
		int pointValue = 10;
		String section = "Section";
		
		try
		{
			new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, section);
			System.out.println("Failure:  InvalidTFQuestionException");
		}
		catch (InvalidTFQuestionException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test11 method
}	//end of Test_TFQuestion class