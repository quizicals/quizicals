package tests;

import background.Constants;
import background.QuizInformation;
import exceptions.InvalidPointsException;
import exceptions.InvalidQuestionsException;
import exceptions.NullQuizNameException;
import exceptions.NullTeacherNameException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_QuizInformation
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 13;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_QuizInformation");
		
		int points = 0;
		
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		points += test5();
		points += test6();
		points += test7();
		points += test8();
		points += test9();
		points += test10();
		points += test11();
		points += test12();
		points += test13();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 4;
		int points = 0;
		
		QuizInformation q = new QuizInformation("Quiz", "Teacher", 100, 10);
		
		//check if the quiz name is valid
		if ("Quiz".equals(q.getQuizName()))	//the quiz name is valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the quiz name is invalid
		{
			System.out.println("Failure:  quizName=\"" + q.getQuizName() + "\"");
		}	//end if
		
		//check if the teacher name is valid
		if ("Teacher".equals(q.getTeacherName()))	//the teacher name is valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the teacher name is invalid
		{
			System.out.println("Failure:  teacherName=\"" + q.getTeacherName() + "\"");
		}	//end if
		
		//check if the total points are valid
		if (q.getTotalPoints() == 100)	//the total points are valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the total points are invalid
		{
			System.out.println("Failure:  totalPoints=" + q.getTotalPoints());
		}	//end if
		
		//check if the number of questions are valid
		if (q.getNumberOfQuestions() == 10)	//the number of questions are valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the number of questions are invalid
		{
			System.out.println("Failure:  numberOfQuestions=" + q.getNumberOfQuestions());
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = null
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation(null, "Teacher", 100, 10);
			System.out.println("Failure:  NullQuizNameException");
		}
		catch (NullQuizNameException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = null
	 * totalPoints = 100
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", null, 100, 10);
			System.out.println("Failure:  NullTeacherNameException");
		}
		catch (NullTeacherNameException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = -1
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", -1, 10);
			System.out.println("Failure:  InvalidPointsException");
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 0
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test5()
	{
		System.out.println("test5");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 0, 10);
			System.out.println("Failure:  InvalidPointsException");
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test5 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = MAX_POINTS
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test6()
	{
		System.out.println("test6");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", Constants.MAX_POINTS, 10);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidPointsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test6 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 1
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test7()
	{
		System.out.println("test7");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 1, 10);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidPointsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test7 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = MAX_POINTS + 1
	 * numberOfQuestions = 10
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test8()
	{
		System.out.println("test8");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", Constants.MAX_POINTS + 1, 10);
			System.out.println("Failure:  InvalidPointsException");
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test8 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = -1
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test9()
	{
		System.out.println("test9");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 100, -1);
			System.out.println("Failure:  InvalidQuestionsException");
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test9 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = 0
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test10()
	{
		System.out.println("test10");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 100, 0);
			System.out.println("Failure:  InvalidQuestionsException");
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test10 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = 1
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test11()
	{
		System.out.println("test11");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 100, 1);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Failure:  InvalidQuestionsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test11 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = MAX_QUESTIONS
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test12()
	{
		System.out.println("test12");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 100, Constants.MAX_QUESTIONS);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Failure:  InvalidQuestionsException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test12 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * teacherName = "Teacher"
	 * totalPoints = 100
	 * numberOfQuestions = MAX_QUESTIONS + 1
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test13()
	{
		System.out.println("test13");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			QuizInformation q = new QuizInformation("Quiz", "Teacher", 100, Constants.MAX_QUESTIONS + 1);
			System.out.println("Failure:  InvalidQuestionsException");
		}
		catch (InvalidQuestionsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test13 method
}	//end of Test_QuizInformation class