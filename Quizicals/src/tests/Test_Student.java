package tests;

import java.util.ArrayList;
import background.QuizScore;
import background.Student;
import exceptions.NullQuizScoresListException;
import exceptions.NullStudentNameException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_Student
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 4;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_Student");
		
		int points = 0;
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * name = "Bob"
	 * quizScores = {"Quiz1", 1} {"Quiz2", 2}
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	public static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int points = 0;
		int maxPoints = 2;
		
		ArrayList<QuizScore> l = new ArrayList<QuizScore>();
		
		l.add(new QuizScore("Quiz1", 1));
		l.add(new QuizScore("Quiz2", 2));
		
		Student s = new Student(0, "Bob", l);
		
		//check if the name is valid
		if ("Bob".equals(s.getName()))	//the name is valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the name is invalid
		{
			System.out.println("Failure:  name=\"" + s.getName() + "\"");
		}	//end if
		
		//check if the list is valid
		if (l.equals(s.getQuizScores()))	//the list is valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the list is invalid
		{
			System.out.print("Failure:  ");
			for (QuizScore qs : l)
			{
				System.out.print("{" + qs.getQuizName() + ", " + qs.getPoints()  + "} ");
			}
			System.out.println();
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * name = "Bob"
	 * quizScores = {"Quiz1", 1}
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	public static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int points = 0;
		int maxPoints = 2;
		
		ArrayList<QuizScore> l = new ArrayList<QuizScore>();
		
		l.add(new QuizScore("Quiz1", 1));
		
		Student s = new Student(0, "Bob", l);
		
		//check if the name is valid
		if ("Bob".equals(s.getName()))	//the name is valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the name is invalid
		{
			System.out.println("Failure:  name=\"" + s.getName() + "\"");
		}	//end if
		
		//check if the list is valid
		if (l.equals(s.getQuizScores()))	//the list is valid
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the list is invalid
		{
			System.out.print("Failure:  ");
			for (QuizScore qs : l)
			{
				System.out.print("{" + qs.getQuizName() + ", " + qs.getPoints()  + "} ");
			}
			System.out.println();
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * name = null
	 * quizScores = {"Quiz1", 1} {"Quiz2", 2}
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	public static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int points = 0;
		int maxPoints = 1;
		
		ArrayList<QuizScore> l = new ArrayList<QuizScore>();
		
		l.add(new QuizScore("Quiz1", 1));
		l.add(new QuizScore("Quiz2", 2));
		
		try
		{
			new Student(0, null, l);
			System.out.println("Failure:  NullStudentNameException");
		}
		catch (NullStudentNameException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * name = "Bob"
	 * quizScores = null
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	public static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int points = 0;
		int maxPoints = 1;
		
		try
		{
			new Student(0, "Bob", null);
			System.out.println("Failure:  NullQuizScoresListException");
		}
		catch (NullQuizScoresListException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
}	//end of Test_Student class