package tests;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_All
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 8;
	
	/**
	 * Runs all the test cases and prints out how many were passed.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		System.out.println("Test_All");
		System.out.println("------------------\n");
		
		int points = 0;
		points += Test_Question.run();
		points += Test_Quiz.run();
		points += Test_QuizInformation.run();
		points += Test_QuizScore.run();
		points += Test_SecurityManager.run();
		points += Test_Student.run();
		points += Test_TFQuestion.run();
		points += Test_Properties.run();
		
		System.out.println("------------------");
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
	}	//end of main method
}	//end of Test_All class