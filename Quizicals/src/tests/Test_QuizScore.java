package tests;

import background.Constants;
import background.QuizScore;
import exceptions.InvalidPointsException;
import exceptions.NullQuizNameException;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class Test_QuizScore
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 6;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_QuizScore");
		
		int points = 0;
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		points += test5();
		points += test6();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * points = 100
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 3;
		int points = 0;
		try
		{
			QuizScore q = new QuizScore("Quiz", 100);
			
			System.out.println("Success");
			points += 1;
			
			//check if the quiz name is valid
			if ("Quiz".equals(q.getQuizName()))	//the name is valid
			{
				System.out.println("Success");
				points += 1;
			}
			else	//the name is invalid
			{
				System.out.println("Failure:  quizName=\"" + q.getQuizName() + "\"");
			}	//end if
			
			//check if the points are valid
			if (q.getPoints() == 100)	//the points are valid
			{
				System.out.println("Success");
				points += 1;
			}
			else	//the points are invalid
			{
				System.out.println("Failure:  points=" + q.getPoints());
			}	//end if
			
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidQuizScoreException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * points = 0
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		try
		{
			QuizScore q = new QuizScore("Quiz", 0);
			System.out.println("Failure:  points=" + q.getPoints());
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * points = 1
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		try
		{
			QuizScore q = new QuizScore("Quiz", 1);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidQuizScoreException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * points = MAX_POINTS
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		try
		{
			QuizScore q = new QuizScore("Quiz", Constants.MAX_POINTS);
			System.out.println("Success");
			points += 1;
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Failure:  InvalidQuizScoreException");
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = "Quiz"
	 * points = MAX_POINTS + 1
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test5()
	{
		System.out.println("test5");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		try
		{
			QuizScore q = new QuizScore("Quiz", Constants.MAX_POINTS + 1);
			System.out.println("Failure:  points=" + q.getPoints());
		}
		catch (InvalidPointsException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test5 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * quizName = null
	 * points = 100
	 * 
	 * @return	Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test6()
	{
		System.out.println("test6");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		try
		{
			new QuizScore(null, 100);
			System.out.println("Failure:  NullQuizNameException");
		}
		catch (NullQuizNameException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test6 method
}	//end of Test_QuizScore class