package tests;

import background.FileIO;
import background.Properties;

public class Test_FileIO {
	
	private static int totalNumPoints = 0;
	
	public static void main(String[] args) {
		if (!FileIO.setShareDirectory(".")) {
			System.out.println("Cannot run FileIO test cases as the share directory cannot be set to the current directory.");
			return;
		}
		
		int points = 0;
		points += testcase1();
		points += testcase2();
		points += testcase3();
		points += testcase4();
		
		System.out.println("Total Score: " + points + " / " + totalNumPoints);

	}
	
	/**
	 * Test case 1: writing an object to file and then reading the file.
	 * @return
	 */
	private static int testcase1() {
		totalNumPoints += 1;
		
		int points = 0;
		String outputString = "test";
		String filename = "testfile";
		
		FileIO.write(filename, outputString);
		String input = (String) FileIO.read(filename);
		
		if (!outputString.equals(input)) {
			System.out.printf("Test case 1 failed. %s != %s\n", outputString, input);
		}
		else {
			points++;
		}
		
		return points;
	}
	
	/**
	 * Test case 2: writing a file using a UNC path, which is more common
	 * @return
	 */
	private static int testcase2() {
		totalNumPoints += 1;
		int points = 0;
		String path = "\\\\cs408\\quizicals";
		Properties.setProperty(path, Properties.DIRECTORY);
		String testString = "this is a test";
		String fileName = "testFile";
		FileIO.write(fileName, testString);
		String readString = (String) FileIO.read(fileName);
		if (testString.equals(readString)) {
			points++;
		}
		else {
			System.out.printf("Test case 2 failed. %s != %s\n", testString, readString);
		}
		
		return points;
		
	}
	
	/**
	 * Test case 3: writing a file that needs to create a directory structure
	 * @return
	 */
	private static int testcase3() {
		totalNumPoints += 1;
		int points = 0;
		
		String path = "\\\\cs408\\quizicals";
		Properties.setProperty(path, Properties.DIRECTORY);
		String testString = "this is a test";
		String fileName = "directory/subdirectory/testFile";
		FileIO.write(fileName, testString);
		String readString = (String) FileIO.read(fileName);
		if (testString.equals(readString)) {
			points++;
		}
		else {
			System.out.printf("Test case 2 failed. %s != %s\n", testString, readString);
		}
		
		return points;
	}
	
	private static int testcase4() {
		totalNumPoints += 1;
		int points = 0;
		
		String notFound = (String) FileIO.read("i/do/not/exist/lol");
		if (notFound == null) {
			++points;
		}
		else {
			return 0;
		}
		
			
		return points;
	}

}
