package tests;

import background.SecurityManager;
import exceptions.NullSecurityException;

public class Test_SecurityManager
{
	/**
	 * Holds the number of test cases.
	 */
	private static final int NUM_TEST_CASES = 4;
	
	/**
	 * Runs the test cases.
	 * 
	 * @param args	Holds the command line parameters.
	 */
	public static void main(String[] args)
	{
		run();
	}	//end of main method
	
	/**
	 * Runs the test cases and returns 1 if all 
	 * test cases were passed or 0 otherwise.
	 * 
	 * @return	Returns 1 if all test cases were passed or 0 otherwise.
	 */
	public static int run()
	{
		System.out.println("Test_SecurityManager");
		
		int points = 0;
		
		points += test1();
		points += test2();
		points += test3();
		points += test4();
		
		System.out.println("Total passed:  " + points + "/" + NUM_TEST_CASES);
		System.out.println();
		
		//check if all the test cases were passed.
		if (points == NUM_TEST_CASES)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of run method
	
	/**
	 * Runs a valid test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * encrypt_info = "test"
	 * decrypt_info = "test"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test1()
	{
		System.out.println("test1");
		System.out.println("------------");
		
		int maxPoints = 3;
		int points = 0;
		
		String s = "test";
		byte[] info = s.getBytes();
		
		byte[] b = SecurityManager.encrypt(info);
		
		//check if the encrypted string is null
		if (b != null)	//the encrypted string  is not null
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the encrypted string is null
		{
			System.out.println("Failure:  encrypt.");
		}	//end if
		
		String decrypted = new String(SecurityManager.decrypt(b));
		
		//check if the decrypted string is null
		if (decrypted != null)	//the decrypted string is not null
		{
			System.out.println("Success");
			points += 1;
		}
		else	//the decrypted string is null
		{
			System.out.println("Failure:  decrypt.");
		}	//end if
		
		//check if the decrypted string is equal to what we started with
		if (s.equals(decrypted))	//they are equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//they are not equal
		{
			System.out.println("Failure:  \"" + info + "\" != \"" + s + "\"");
		}	//end if
		
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test1 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * encrypt_info = null
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test2()
	{
		System.out.println("test2");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		byte[] info = null;
		
		try
		{
			SecurityManager.encrypt(info);
			System.out.println("Failure:  NullSecurityException");
		}
		catch (NullSecurityException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
			
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test2 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * decrypt_info = null 
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test3()
	{
		System.out.println("test3");
		System.out.println("------------");
		
		int maxPoints = 1;
		int points = 0;
		
		byte[] info = null;
		
		try
		{
			SecurityManager.decrypt(info);
			System.out.println("Failure:  NullSecurityException");
		}
		catch (NullSecurityException e)
		{
			System.out.println("Success");
			points += 1;
		}	//end try
			
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test3 method
	
	/**
	 * Runs a boundary test case and returns 1 if 
	 * everything succeeded and 0 otherwise.
	 * 
	 * info1 = "test"
	 * info2 = "test2"
	 * 
	 * @return	 Returns 1 if everything succeeded and 0 otherwise.
	 */
	private static int test4()
	{
		System.out.println("test4");
		System.out.println("------------");
		
		int maxPoints = 2;
		int points = 0;
		
		String info1 = "test";
		String info2 = "test2";
		
		byte[] b1 = SecurityManager.encrypt(info1.getBytes());
		byte[] b2 = SecurityManager.encrypt(info2.getBytes());
		
		//check if the bytes are equal
		if (!b1.equals(b2))	//the bytes are not equal
		{
			System.out.println("Success");
			points += 1;
		}
		else
		{
			System.out.println("Failure:  Different encrypts are equal.");
		}	//end if
		
		String s1 = new String(SecurityManager.decrypt(b1));
		String s2 = new String(SecurityManager.decrypt(b2));
		
		//check if the strings are equal
		if (!s1.equals(s2))	//strings are not equal
		{
			System.out.println("Success");
			points += 1;
		}
		else	//strings are equal
		{
			System.out.println("Failure:  Different decrypts are equal.");
		}	//end if
			
		System.out.println("------------");
		System.out.println("Passed:  " + points + "/" + maxPoints + "\n");
		
		//check if all the test cases were passed.
		if (points == maxPoints)	//all test cases were passed
		{
			return 1;
		}	//end if
		
		return 0;
	}	//end of test4 method
}	//end of Test_SecurityManager class