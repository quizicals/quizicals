package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import background.Constants;
import background.QuizManager;
import background.User;
import database.Database;

public class TeacherClassSelectForm extends JFrame
{
	private final TeacherClassSelectForm CLASS_SELECT_FORM = this;
	public enum Mode {Create, Select};
	
	public TeacherClassSelectForm(Mode mode)
	{
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel labelPanel = new JPanel();
		labelPanel.add(new JLabel("Select a Class"));
		superPanel.add(labelPanel);
		
		Database db = Database.getInstance();
		ResultSet rs = db.query("SELECT * FROM classes WHERE teacherID=\'" + User.getCurrentUser().getUID() + "\'");
		
		try
		{
			JPanel classesPanel = new JPanel();
			classesPanel.setLayout(new GridLayout(0, 1));
			
			while (rs.next())
			{
				JClassButton classButton = new JClassButton(rs.getString("className"), rs.getInt("classID"));
				
				classButton.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							QuizManager.classID = classButton.ID;
							switch (mode)
							{
							case Select:
								new TeacherQuizSelectForm(mode);
								break;
							case Create:
								new QuestionForm(mode);
								break;
							}
							
							CLASS_SELECT_FORM.dispose();
						}
					});
				
				classesPanel.add(classButton);
			}
			
			rs.close();
			JScrollPane scrollPane = new JScrollPane(classesPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			superPanel.add(scrollPane);
		}
		catch (Exception e)
		{
			
		}
		
		db.close();
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		JButton backButton = new JButton("Back");
		
		backButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						new TeacherMenuForm();
						CLASS_SELECT_FORM.dispose();
					}
				});
		
		buttonPanel.add(backButton);
		superPanel.add(buttonPanel);
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(new Dimension(300, 300));
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new TeacherClassSelectForm(Mode.Select);
	}
}