package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import background.Constants;
import background.FileIO;
import background.Question;
import background.Quiz;
import background.QuizInformation;
import background.QuizManager;
import background.User;
import background.UserType;
import database.Database;

public class TeacherQuizForm extends JFrame
{
	private final TeacherQuizForm QUIZ_FORM = this;

	public TeacherQuizForm(TeacherClassSelectForm.Mode mode)
	{
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel intermediatePanel = new JPanel();
		intermediatePanel.setLayout(new BoxLayout(intermediatePanel, BoxLayout.Y_AXIS));
		
		for (int x = 0; x < QuizManager.questions.size(); x += 1)
		{
			JPanel questionPanel = new JPanel();
			//questionPanel.setLayout(new BoxLayout(questionPanel, BoxLayout.Y_AXIS));
			questionPanel.setLayout(new BorderLayout());
			questionPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
			JLabel questionLabel = new JLabel("" + (x + 1) + ". " + QuizManager.questions.get(x).getQuestionString());
			//questionLabel.setFont(new Font(questionLabel.getFont().getFontName(), Font.BOLD, questionLabel.getFont().getSize() + 2));
			//questionPanel.add(questionLabel);
			questionPanel.add(questionLabel, BorderLayout.PAGE_START);
			
			JPanel answerPanel = new JPanel();
			answerPanel.setLayout(new GridLayout(0, 1));
			questionPanel.add(answerPanel, BorderLayout.CENTER);
			for (int y = 0; y < QuizManager.questions.get(x).getAnswers().size(); y += 1)
			{
				JLabel answerLabel = new JLabel("    " + (char)('a' + y) + ".  " + QuizManager.questions.get(x).getAnswers().get(y));
				
				/*if (QuizManager.questions.get(x).getCorrectAnswer().equals(QuizManager.questions.get(x).getAnswers().get(y)))
				{
					answerLabel.setText("*" + answerLabel.getText());
				}
				else
				{
					answerLabel.setText("  " + answerLabel.getText());
				}*/
				if (QuizManager.questions.get(x).getCorrectAnswer().equals(QuizManager.questions.get(x).getAnswers().get(y)))
				{
					answerLabel.setFont(new Font(answerLabel.getFont().getFontName(), Font.BOLD, answerLabel.getFont().getSize() + 2));
				}
				
				//questionPanel.add(answerLabel);
				answerPanel.add(answerLabel);
			}
			
			JPanel questionButtonPanel = new JPanel();
			questionButtonPanel.setLayout(new BorderLayout());
			JEditButton editButton = new JEditButton(x);
			JRemoveButton removeButton = new JRemoveButton(x);
			
			editButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						new QuestionForm(editButton.INDEX, mode);
						QUIZ_FORM.dispose();
					}
				});
			
			removeButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						QuizManager.questions.remove(removeButton.INDEX);						
						new TeacherQuizForm(mode);
						QUIZ_FORM.dispose();
					}
				});
			
			JPanel why = new JPanel();
			why.add(editButton);
			why.add(removeButton);
			questionButtonPanel.add(why, BorderLayout.WEST);
			//questionPanel.add(questionButtonPanel);
			questionPanel.add(questionButtonPanel, BorderLayout.PAGE_END);
			intermediatePanel.add(questionPanel);
		}
		
		JScrollPane scrollPane = new JScrollPane(intermediatePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(60);
		superPanel.add(scrollPane);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		JButton addButton = new JButton("Add a Question");
		JButton finishButton = new JButton("Finish");
		JButton cancelButton = new JButton("Cancel");
		JButton backButton =  new JButton("Back");
		
		addButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new QuestionForm(mode);
					QUIZ_FORM.dispose();
				}
			});
		
		finishButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					//don't finish if there are no questions
					if(QuizManager.questions.size() == 0)
					{
						JOptionPane.showMessageDialog(QUIZ_FORM, "Quiz must have at least one question.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					//declare variables
					String quizName = "";
					int quizID;
					
					if(QuizManager.quizInfo == null)
					{
						//name quiz
						quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
						if(quizName == null)
						{
							return;
						}
						while(quizName.equals(""))
						{
							quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
							if(quizName == null)
							{
								return;
							}
						}
						Database db = Database.getInstance();
						try
						{
							ResultSet results = db.query("SELECT * FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
							//invalid quizName for this classID
							while (results.next())
							{
								//name quiz
								JOptionPane.showMessageDialog(QUIZ_FORM, "Quiz name already exists for this class.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
								quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
								if(quizName == null)
								{
									return;
								}
								while(quizName.equals(""))
								{
									quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
									if(quizName == null)
									{
										return;
									}
								}
								results.close();
								results = db.query("SELECT * FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
							}
							
							results.close();
						}
						catch(SQLException ex)
						{
							ex.printStackTrace();
							JOptionPane.showMessageDialog(QUIZ_FORM, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						//insert into database
						db.query("INSERT INTO quizzes (quizName, classID) VALUES ('" + quizName + "', " + QuizManager.classID + ")");
						db.close();
					}
					else
					{
						//get quizName
						quizName = QuizManager.quizInfo.getQuizName();
					}
					
					//get quizID
					Database db = Database.getInstance();
					try
					{
						ResultSet rs = db.query("SELECT quizID FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
						rs.next();
						quizID = rs.getInt("quizID");
						rs.close();
					}
					catch (SQLException ex)
					{
						ex.printStackTrace();
						JOptionPane.showMessageDialog(QUIZ_FORM, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
						db.close();
						return;
					}
					db.close();
					
					//location of the quiz
					String location = "" + QuizManager.classID + "/" + quizID + ".q";
					
					int points = 0;
					for(Question q : QuizManager.questions)
					{
						points += q.getPointValue();
					}
					
					User u = User.getCurrentUser();
					QuizManager.quizInfo = new QuizInformation(quizName, u.getFullName(), points, QuizManager.questions.size());
					Quiz q = new Quiz(QuizManager.questions, location, QuizManager.quizInfo);

					FileIO.write(location, q);

					QuizManager.clear();
					new TeacherMenuForm();
					QUIZ_FORM.dispose();
				}
			});
		
		cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (hasChanges())
					{
						int result = JOptionPane.showConfirmDialog(QUIZ_FORM, "Are you sure you want to cancel?", Constants.TITLE, JOptionPane.YES_NO_OPTION);
						
						if (result == JOptionPane.YES_OPTION)
						{
							QuizManager.clear();
							new TeacherMenuForm();
							QUIZ_FORM.dispose();
						}
					}
					else
					{
						QuizManager.clear();
						new TeacherMenuForm();
						QUIZ_FORM.dispose();
					}
				}
			});
		
		backButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (hasChanges())
					{
						int result = JOptionPane.showConfirmDialog(QUIZ_FORM, "Would you like to save your changes to the quiz?", Constants.TITLE, JOptionPane.YES_NO_OPTION);
						
						if (result == JOptionPane.YES_OPTION)
						{
							//don't finish if there are no questions
							if(QuizManager.questions.size() == 0)
							{
								JOptionPane.showMessageDialog(QUIZ_FORM, "Quiz must have at least one question.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
								return;
							}
							
							//declare variables
							String quizName = "";
							int quizID;
							
							if(QuizManager.quizInfo == null)
							{
								//name quiz
								quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
								if(quizName == null)
								{
									return;
								}
								while(quizName.equals(""))
								{
									quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
									if(quizName == null)
									{
										return;
									}
								}
								Database db = Database.getInstance();
								try
								{
									ResultSet results = db.query("SELECT * FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
									//invalid quizName for this classID
									while (results.next())
									{
										//name quiz
										JOptionPane.showMessageDialog(QUIZ_FORM, "Quiz name already exists for this class.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
										quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
										if(quizName == null)
										{
											return;
										}
										while(quizName.equals(""))
										{
											quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
											if(quizName == null)
											{
												return;
											}
										}
										results.close();
										results = db.query("SELECT * FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
									}
									
									results.close();
								}
								catch(SQLException ex)
								{
									ex.printStackTrace();
									JOptionPane.showMessageDialog(QUIZ_FORM, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
									return;
								}
								
								//insert into database
								db.query("INSERT INTO quizzes (quizName, classID) VALUES ('" + quizName + "', " + QuizManager.classID + ")");
								db.close();
							}
							else
							{
								//get quizName
								quizName = QuizManager.quizInfo.getQuizName();
							}
							
							//get quizID
							Database db = Database.getInstance();
							try
							{
								ResultSet rs = db.query("SELECT quizID FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
								rs.next();
								quizID = rs.getInt("quizID");
								rs.close();
							}
							catch (SQLException ex)
							{
								ex.printStackTrace();
								JOptionPane.showMessageDialog(QUIZ_FORM, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
								db.close();
								return;
							}
							db.close();
							
							//location of the quiz
							String location = "" + QuizManager.classID + "/" + quizID + ".q";
							
							int points = 0;
							for(Question q : QuizManager.questions)
							{
								points += q.getPointValue();
							}
							
							User u = User.getCurrentUser();
							QuizManager.quizInfo = new QuizInformation(quizName, u.getFullName(), points, QuizManager.questions.size());
							Quiz q = new Quiz(QuizManager.questions, location, QuizManager.quizInfo);

							FileIO.write(location, q);
						}
						else if (result == JOptionPane.CLOSED_OPTION)
						{
							return;
						}
					}
						
					if (mode == TeacherClassSelectForm.Mode.Select)
					{
						QuizManager.quizID = -1;
						new TeacherQuizSelectForm(mode);
					}
					else
					{
						QuizManager.clear();
						new TeacherMenuForm();
					}
					
					QUIZ_FORM.dispose();
				}
			});
		
		buttonPanel.add(addButton);
		buttonPanel.add(cancelButton);
		buttonPanel.add(backButton);
		buttonPanel.add(finishButton);
		superPanel.add(buttonPanel);
		
		this.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				if (hasChanges())
				{
					int result = JOptionPane.showConfirmDialog(QUIZ_FORM, "Would you like to save your changes to the quiz?", Constants.TITLE, JOptionPane.YES_NO_OPTION);
					
					if (result == JOptionPane.YES_OPTION)
					{
						//don't finish if there are no questions
						if(QuizManager.questions.size() == 0)
						{
							JOptionPane.showMessageDialog(QUIZ_FORM, "Quiz must have at least one question.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
							QUIZ_FORM.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
							return;
						}
						
						//declare variables
						String quizName = "";
						int quizID;
						
						if(QuizManager.quizInfo == null)
						{
							//name quiz
							quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
							if(quizName == null)
							{
								return;
							}
							while(quizName.equals(""))
							{
								quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
								if(quizName == null)
								{
									return;
								}
							}
							Database db = Database.getInstance();
							try
							{
								ResultSet results = db.query("SELECT * FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
								//invalid quizName for this classID
								while (results.next())
								{
									//name quiz
									JOptionPane.showMessageDialog(QUIZ_FORM, "Quiz name already exists for this class.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
									quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
									if(quizName == null)
									{
										return;
									}
									while(quizName.equals(""))
									{
										quizName = JOptionPane.showInputDialog(QUIZ_FORM, "Enter Quiz Name:", Constants.TITLE, JOptionPane.OK_CANCEL_OPTION);
										if(quizName == null)
										{
											return;
										}
									}
									results.close();
									results = db.query("SELECT * FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
								}
								
								results.close();
							}
							catch(SQLException ex)
							{
								ex.printStackTrace();
								JOptionPane.showMessageDialog(QUIZ_FORM, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
								return;
							}
							
							//insert into database
							db.query("INSERT INTO quizzes (quizName, classID) VALUES ('" + quizName + "', " + QuizManager.classID + ")");
							db.close();
						}
						else
						{
							//get quizName
							quizName = QuizManager.quizInfo.getQuizName();
						}
						
						//get quizID
						Database db = Database.getInstance();
						try
						{
							ResultSet rs = db.query("SELECT quizID FROM quizzes WHERE classID=" + QuizManager.classID + " AND quizName='" + quizName + "'");
							rs.next();
							quizID = rs.getInt("quizID");
							rs.close();
						}
						catch (SQLException ex)
						{
							ex.printStackTrace();
							JOptionPane.showMessageDialog(QUIZ_FORM, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
							db.close();
							return;
						}
						db.close();
						
						//location of the quiz
						String location = "" + QuizManager.classID + "/" + quizID + ".q";
						
						int points = 0;
						for(Question q : QuizManager.questions)
						{
							points += q.getPointValue();
						}
						
						User u = User.getCurrentUser();
						QuizManager.quizInfo = new QuizInformation(quizName, u.getFullName(), points, QuizManager.questions.size());
						Quiz q = new Quiz(QuizManager.questions, location, QuizManager.quizInfo);
	
						FileIO.write(location, q);
					}
					else if (result == JOptionPane.CLOSED_OPTION)
					{
						QUIZ_FORM.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
						return;
					}
				}
				
				QUIZ_FORM.dispose();
			}
		});
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		int width = 16 + (int)(addButton.getPreferredSize().getWidth() + cancelButton.getPreferredSize().getWidth() + backButton.getPreferredSize().getWidth() + finishButton.getPreferredSize().getWidth());
		//this.setSize(new Dimension(width, 300));
		this.setMinimumSize(new Dimension(width, 300));
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private boolean hasChanges()
	{
		if (QuizManager.quizInfo == null)
		{
			if (QuizManager.questions.size() == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			Quiz quiz = (Quiz)FileIO.read(QuizManager.classID + "/" + QuizManager.quizID + ".q");
			LinkedList<Question> questions = quiz.getQuestions();
			
			if (questions.size() != QuizManager.questions.size())
			{
				return true;
			}
			else
			{
				for (int x = 0; x < questions.size(); x += 1)
				{
					if (!questions.get(x).equals(QuizManager.questions.get(x)))
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public static void main(String[] args)
	{
		User.setCurrentUser(UserType.teacher, "Bob", "Ross", "bross", 1);
		QuizManager.classID = 3;
		new TeacherQuizForm(TeacherClassSelectForm.Mode.Create);
	}
}