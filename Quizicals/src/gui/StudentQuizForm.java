package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import background.CompletedQuiz;
import background.Constants;
import background.FileIO;
import background.Question;
import background.Quiz;
import background.QuizManager;
import background.User;
import database.Database;

public class StudentQuizForm extends JFrame
{
	private final StudentQuizForm STUDENT_QUIZ = this;
	private JPanel innerPanel;
	
	private LinkedList<ButtonGroup> answerGroups = new LinkedList<ButtonGroup>();
	
	private Quiz quiz;
	private LinkedList<Question> questions;

    public StudentQuizForm()
    {
    	this.quiz = new Quiz(QuizManager.questions, "" + QuizManager.classID + "/" + QuizManager.quizID + "/" + User.getCurrentUser().getUID() + ".cq", QuizManager.quizInfo);
    	this.questions = quiz.getRandomizedQuestions();
    	//set this form's layout
    	this.innerPanel = new JPanel();
    	this.innerPanel.setLayout(new BoxLayout(this.innerPanel, BoxLayout.Y_AXIS));
    	JPanel intermediatePanel = new JPanel();
    	intermediatePanel.setLayout(new GridLayout(0, 1));
    	int questionNumber = 1;
        
        //display all questions and answers
        for(Question question : this.questions)
        {
        	JPanel questionPanel = new JPanel();
        	questionPanel.setLayout(new GridLayout(0,1));
        	JLabel questionLabel = new JLabel("" + questionNumber + ".  " + question.getQuestionString());
        	questionPanel.add(questionLabel);
        	
        	//display answers to this question
        	ButtonGroup answerButtons = new ButtonGroup();
        	LinkedList<String> answers = question.getRandomizedAnswers();
        	char answerLetter = 'a';
        	for(String answer : answers)
        	//for(String answer : question.getAnswers())
        	{
        		JRadioButton answerButton = new JRadioButton("" + answerLetter + ".  " + answer);
        		//this.answerstr.add(answer);
        		this.answerGroups.add(answerButtons);
        		questionPanel.add(answerButton);
        		answerButtons.add(answerButton);
        		
        		answerLetter++;
        	}
        	
        	questionNumber++;
        	intermediatePanel.add(questionPanel);
        }
        
        JScrollPane scrollPane = new JScrollPane(intermediatePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(60);
        innerPanel.add(scrollPane);
        
        //db.close();

        JButton submiButton = new JButton("Submit");
        submiButton.addActionListener(new ActionListener()
	        {
	            @Override
	            public void actionPerformed(ActionEvent e)
	            {
	            	int score = 0;
	            	LinkedList<String> selected = new LinkedList<String>();

	            	for(int x = 0; x < intermediatePanel.getComponentCount(); x++)
	            	{
	            		JPanel questionPanel = (JPanel)intermediatePanel.getComponent(x);
	            		
	            		for(int y = 1; y < questionPanel.getComponentCount(); y++)
	            		{
	            			JRadioButton button = (JRadioButton)questionPanel.getComponent(y);
	            			if(button.isSelected())
	            			{
	            				selected.add(button.getText().substring(4));
	    	            		
	    	            		//check if answer is correct
	    	            		Question q = STUDENT_QUIZ.questions.get(x);
	    	            		if(q.isCorrect(button.getText().substring(4)))
	    	            		{
	    	            			score += q.getPointValue();
	    	            		}
	    	            		
	    	            		break;
	            			}
	            		}
	            	}
	                
	                //save completed quiz
	            	if (STUDENT_QUIZ.questions.size() != selected.size())
	            	{
	            		int unanswered = STUDENT_QUIZ.questions.size() - selected.size();
	            		JOptionPane.showMessageDialog(STUDENT_QUIZ, "You have " + unanswered + " unanswered questions.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
	            		return;
	            	}

	            	CompletedQuiz taken = new CompletedQuiz(STUDENT_QUIZ.quiz, STUDENT_QUIZ.questions, selected);
	                FileIO.write("" + QuizManager.classID + "/" + QuizManager.quizID + "/" + User.getCurrentUser().getUID() + ".cq", taken);
	            	
	                Database db = Database.getInstance();
	                try
	                {
	                	//get enrollmentID
	                	ResultSet rs = db.query("SELECT enrollmentID FROM enrollment WHERE classID=" + QuizManager.classID + " AND studentID=" + User.getCurrentUser().getUID());
	                	rs.next();
	                	int enrollmentID = rs.getInt("enrollmentID");
	                	rs.close();
	                	
	                	//insert new score
	                	db.query("INSERT INTO scores (quizID, enrollmentID, studentID, score) VALUES "
	                			+ "(" + QuizManager.quizID + ","
	                			+ enrollmentID + ","
	                			+ User.getCurrentUser().getUID() +  ","
	                			+ score + ")");
	                }
	                catch(SQLException ex)
	                {
	                	ex.printStackTrace();
	                }
	                db.close();

	                showGrade(score);
	                QuizManager.clear();
	                new StudentMenuForm();
	                STUDENT_QUIZ.dispose();
	            }
	        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(submiButton);
        
        innerPanel.add(buttonPanel);

        this.addWindowListener(new WindowAdapter()
        		{
        			public void windowClosing(WindowEvent e)
        			{
        				int result = JOptionPane.showConfirmDialog(STUDENT_QUIZ, "If you leave now, your responses will be submitted.", Constants.TITLE, JOptionPane.WARNING_MESSAGE);
        				
        				if (result != JOptionPane.OK_OPTION)
        				{
        					STUDENT_QUIZ.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        					return;
        				}
        				
        				int score = 0;
    	            	LinkedList<String> selected = new LinkedList<String>();
    	                
    	            	for(int x = 0; x < intermediatePanel.getComponentCount(); x++)
    	            	{
    	            		JPanel questionPanel = (JPanel)intermediatePanel.getComponent(x);
    	            		boolean found = false;
    	            		
    	            		for(int y = 1; y < questionPanel.getComponentCount(); y++)
    	            		{
    	            			JRadioButton button = (JRadioButton)questionPanel.getComponent(y);
    	            			if(button.isSelected())
    	            			{
    	            				selected.add(button.getText().substring(4));
    	    	            		
    	    	            		//check if answer is correct
    	    	            		Question q = STUDENT_QUIZ.questions.get(x);
    	    	            		if(q.isCorrect(button.getText().substring(4)))
    	    	            		{
    	    	            			score += q.getPointValue();
    	    	            		}
    	    	            		
    	    	            		found = true;
    	    	            		break;
    	            			}
    	            		}
    	            		
    	            		if (!found)
    	            		{
    	            			selected.add(null);
    	            		}
    	            	}

    	            	CompletedQuiz taken = new CompletedQuiz(STUDENT_QUIZ.quiz, STUDENT_QUIZ.questions, selected);
    	                FileIO.write("" + QuizManager.classID + "/" + QuizManager.quizID + "/" + User.getCurrentUser().getUID() + ".cq", taken);
    	            	
    	                Database db = Database.getInstance();
    	                try
    	                {
    	                	//get enrollmentID
    	                	ResultSet rs = db.query("SELECT enrollmentID FROM enrollment WHERE classID=" + QuizManager.classID + " AND studentID=" + User.getCurrentUser().getUID());
    	                	rs.next();
    	                	int enrollmentID = rs.getInt("enrollmentID");
    	                	rs.close();
    	                	
    	                	//insert new score
    	                	db.query("INSERT INTO scores (quizID, enrollmentID, studentID, score) VALUES "
    	                			+ "(" + QuizManager.quizID + ","
    	                			+ enrollmentID + ","
    	                			+ User.getCurrentUser().getUID() +  ","
    	                			+ score + ")");
    	                }
    	                catch(SQLException ex)
    	                {
    	                	ex.printStackTrace();
    	                }
    	                db.close();
    	                
    	                showGrade(score);
    	                STUDENT_QUIZ.dispose();
        			}
        		});
        
        this.add(innerPanel);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(new Dimension(300, 300));
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

    }
    
    private void showGrade(int theirScore)
    {
    	int totalScore = 0;
    	
    	for (Question q : QuizManager.questions)
    	{
    		totalScore += q.getPointValue();
    	}
    	
    	JOptionPane.showMessageDialog(STUDENT_QUIZ, "You scored " + theirScore + " out of " + totalScore + " points.", Constants.TITLE, JOptionPane.INFORMATION_MESSAGE);
    }
}
