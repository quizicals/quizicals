package gui;

import javax.swing.JButton;

public class JClassButton extends JButton
{
	public final int ID;
	
	public JClassButton(String text, int id)
	{
		super(text);
		this.ID = id;
	}
}