
package gui;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;

import background.Constants;
import background.FileIO;
import background.Quiz;
import background.QuizManager;
import database.Database;

public class TeacherMenuForm extends JFrame {
	private TeacherMenuForm TEACHER_WINDOW = this;
	
    public TeacherMenuForm() {
        // TODO Auto-generated constructor stub
        JButton createQuizButton = new JButton("Create Quiz");
        add(createQuizButton);

        JButton editOldQuizButton = new JButton("Edit old Quiz");
        add(editOldQuizButton);

        JButton viewStudentScoreButton = new JButton("View Student Scores");//including edit
        add(viewStudentScoreButton);


        createQuizButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
            	new TeacherClassSelectForm(TeacherClassSelectForm.Mode.Create);
                dispose();
            }
        });
        
        editOldQuizButton.addActionListener(new ActionListener()
        		{
		        	public void actionPerformed(ActionEvent e) {
		        		new TeacherClassSelectForm(TeacherClassSelectForm.Mode.Select);
		                TEACHER_WINDOW.dispose();
		            }
        		});

        viewStudentScoreButton.addActionListener(new ActionListener()
			{
	        	public void actionPerformed(ActionEvent e)
	        	{
	        		new GradebookForm();
	        		TEACHER_WINDOW.dispose();
	        	}
			});
        
        this.setLayout(new FlowLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static void main(String[] args)
    {
    	new TeacherMenuForm();
    }
}
