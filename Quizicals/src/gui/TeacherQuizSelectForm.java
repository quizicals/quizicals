package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.ResultSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import background.Constants;
import background.FileIO;
import background.Quiz;
import background.QuizManager;
import database.Database;

public class TeacherQuizSelectForm extends JFrame
{
	private final TeacherQuizSelectForm QUIZ_SELECT_FORM = this;
	
	public TeacherQuizSelectForm(TeacherClassSelectForm.Mode mode)
	{
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel labelPanel = new JPanel();
		labelPanel.add(new JLabel("Select a Quiz"));
		superPanel.add(labelPanel);
		
		Database db = Database.getInstance();
		ResultSet rs = db.query("SELECT * FROM quizzes WHERE classID=\'" + QuizManager.classID + "\'");
		
		try
		{
			JPanel quizzesPanel = new JPanel();
			quizzesPanel.setLayout(new GridLayout(0, 1));
			
			while (rs.next())
			{
				if (!FileIO.exists(QuizManager.classID + "/" + rs.getInt("quizID") + ".q"))
				{
					continue;
				}
				
				JQuizButton quizButton = new JQuizButton(rs.getString("quizName"), rs.getInt("quizID"));
				
				quizButton.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
			        		Quiz quiz = (Quiz)FileIO.read(QuizManager.classID + "/" + quizButton.ID + ".q");
			        		QuizManager.questions = quiz.getQuestions();
			        		QuizManager.quizInfo = quiz.getQuizInformation();
			        		QuizManager.quizID = quizButton.ID;

							new TeacherQuizForm(mode);
							QUIZ_SELECT_FORM.dispose();
						}
					});
				
				quizzesPanel.add(quizButton);
			}
			
			rs.close();
			JScrollPane scrollPane = new JScrollPane(quizzesPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			superPanel.add(scrollPane);
		}
		catch (Exception e)
		{
			
		}
		db.close();
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		JButton backButton = new JButton("Back");
		
		backButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						QuizManager.clear();
						new TeacherClassSelectForm(mode);
						QUIZ_SELECT_FORM.dispose();
					}
				});
		
		buttonPanel.add(backButton);
		superPanel.add(buttonPanel);
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(new Dimension(300, 300));
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
	}
}