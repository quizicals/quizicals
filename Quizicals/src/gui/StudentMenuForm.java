package gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import background.Constants;

public class StudentMenuForm extends JFrame
{
	private final StudentMenuForm STUDENT_MENU_FORM = this;
	
	public StudentMenuForm()
	{
		JButton takeButton = new JButton("Take Quiz");
		JButton viewButton = new JButton("View Old Quiz");
		
		takeButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						new StudentClassSelectForm(StudentQuizSelectForm.Mode.New);
						STUDENT_MENU_FORM.dispose();
					}
				});
		
		viewButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new StudentClassSelectForm(StudentQuizSelectForm.Mode.Old);
					STUDENT_MENU_FORM.dispose();
				}
			});
		
		this.add(takeButton);
		this.add(viewButton);
		
		this.setLayout(new FlowLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
	}
}