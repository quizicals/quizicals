package gui;

import javax.swing.JTextField;

public class JIDTextField extends JTextField
{
	public final int quizID;
	public final int studentID;
	
	public JIDTextField(String text, int quizID, int studentID)
	{
		super(text);
		this.quizID = quizID;
		this.studentID = studentID;
		this.setColumns(3);
	}
}