package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import background.Constants;
import background.UserType;
import database.Database;

public class PasswordResetForm extends JFrame
{
	private final PasswordResetForm PASSWORD_RESET_FORM = this;
	
	public PasswordResetForm(UserType userType, String username)
	{
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel newPanel = new JPanel();
		newPanel.setLayout(new BoxLayout(newPanel, BoxLayout.X_AXIS));
		JPanel confirmPanel = new JPanel();
		confirmPanel.setLayout(new BoxLayout(confirmPanel, BoxLayout.X_AXIS));
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		JLabel newLabel = new JLabel("New password:        ");
		JLabel confirmLabel = new JLabel ("Confirm password:  ");
		JPasswordField newField = new JPasswordField();
		JPasswordField confirmField = new JPasswordField();
		JButton resetButton = new JButton("Reset Password");
		JButton cancelButton = new JButton("Cancel");
		
		//check if fields are equal
		//make sure they are in the proper length
		
		resetButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String password = newField.getText();
				
				if (password.equals(confirmField.getText()))
				{
					if (password.length() >= Constants.MIN_PASSWORD_LENGTH && password.length() <= Constants.MAX_PASSWORD_LENGTH)
					{
						Database database = Database.getInstance();
						ResultSet rs;
						
						if (userType == UserType.student)
						{
							rs = database.query("UPDATE students SET password=\'" + password + "\' WHERE username=\'" + username + "\'");
						}
						else
						{
							rs = database.query("UPDATE teachers SET password=\'" + password + "\' WHERE username=\'" + username + "\'");
						}
						
						try
						{
							rs.close();
						}
						catch (Exception ex)
						{
							
						}
						
						database.close();
						
						JOptionPane.showMessageDialog(PASSWORD_RESET_FORM, "Password reset", Constants.TITLE, JOptionPane.INFORMATION_MESSAGE);
						new LoginForm();
						PASSWORD_RESET_FORM.dispose();
						return;
					}
				}
				else
				{
					JOptionPane.showMessageDialog(PASSWORD_RESET_FORM, "Your passwords do not match.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				JOptionPane.showMessageDialog(PASSWORD_RESET_FORM, "Invalid password:  Password needs to be between " + Constants.MIN_PASSWORD_LENGTH + " and " + Constants.MAX_PASSWORD_LENGTH + " characters.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
			}
		});
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new LoginForm();
				PASSWORD_RESET_FORM.dispose();
			}
		});
		
		newPanel.add(newLabel);
		newPanel.add(newField);
		
		confirmPanel.add(confirmLabel);
		confirmPanel.add(confirmField);
		
		buttonPanel.add(resetButton);
		buttonPanel.add(cancelButton);
		
		superPanel.add(newPanel);
		superPanel.add(confirmPanel);
		superPanel.add(buttonPanel);
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new PasswordResetForm(UserType.student, "mwaz");
	}
}