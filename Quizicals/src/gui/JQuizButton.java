package gui;

import javax.swing.JButton;

public class JQuizButton extends JButton
{
	public final int ID;
	
	public JQuizButton(String text, int id)
	{
		super(text);
		this.ID = id;
	}
}
