package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import background.Constants;
import background.Question;
import background.QuizManager;
import background.TFQuestion;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class QuestionForm extends JFrame
{
	private String questionString;
	private LinkedList<String> answerStrings;
	private int pointValue;
	private String correctAnswer;
	private int numAnswers = 0;
	private final QuestionForm QUESTION_FORM = this;
	
	public QuestionForm(TeacherClassSelectForm.Mode mode)
	{
		questionString = null;
		correctAnswer = null;
		answerStrings = new LinkedList<String>();
		pointValue = 10;
		
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		
		JPanel questionPanel = new JPanel();
		JLabel questionLabel = new JLabel("Question");
		JTextField questionField = new JTextField();
		JLabel answerLabel = new JLabel("Answers");
		JPanel lolzPanel = new JPanel();
		JCheckBox tfCheckBox = new JCheckBox("True/False");
		
		lolzPanel.setLayout(new BoxLayout(lolzPanel, BoxLayout.X_AXIS));
		lolzPanel.add(answerLabel);
		lolzPanel.add(tfCheckBox);
		questionPanel.add(questionLabel);
		questionPanel.add(questionField);
		questionPanel.add(lolzPanel);
		questionPanel.setLayout(new BoxLayout(questionPanel, BoxLayout.Y_AXIS));
		superPanel.add(questionPanel);
		
		JPanel answerPanel = new JPanel();
		answerPanel.setLayout(new BoxLayout(answerPanel, BoxLayout.Y_AXIS));
		ButtonGroup answerGroup = new ButtonGroup();
		superPanel.add(answerPanel);
		
		JPanel buttonPanel = new JPanel();
		JButton answerButton = new JButton("Add Answer");
		JButton questionButton = new JButton("Add Question");
		JButton finishButton = new JButton("Finish");
		JButton cancelButton = new JButton("Cancel");
		
		answerButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						if (numAnswers == Constants.MAX_ANSWERS)
						{
							return;
						}
						
						numAnswers += 1;
						JPanel panel = new JPanel();
						panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
						JRadioButton radio = new JRadioButton();
						JTextField text = new JTextField();
						answerGroup.add(radio);
						panel.add(radio);
						panel.add(text);
						answerPanel.add(panel);
						QUESTION_FORM.pack();
					}
				});
		
		questionButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (!"".equals(questionField.getText()))
					{
						int num = 0;
						for (int x = 0; x < answerPanel.getComponentCount(); x += 1)
						{
							if (tfCheckBox.isSelected() || !"".equals(((JTextField)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText()))
							{
								num += 1;
								
								if (tfCheckBox.isSelected())
								{
									answerStrings.add(((JLabel)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText());
								}
								else
								{
									answerStrings.add(((JTextField)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText());
								}
								
								if (((JRadioButton)((JPanel)answerPanel.getComponent(x)).getComponent(0)).isSelected())
								{
									correctAnswer = answerStrings.get(x);
								}
							}
						}
						
						if (num < 2 || correctAnswer == null)
						{
							answerStrings.clear();
							correctAnswer = null;
							JOptionPane.showMessageDialog(QUESTION_FORM, "Incorrect Format", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						questionString = questionField.getText();
						Question question;
						
						if (tfCheckBox.isSelected())
						{
							question = new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, "1");
						}
						else
						{
							question = new Question(questionString, answerStrings, correctAnswer, pointValue, "1");
						}
						
						QuizManager.questions.add(question);
						
						if (QuizManager.questions.size() == Constants.MAX_QUESTIONS)
						{
							finishButton.doClick();
						}
						else
						{
							new QuestionForm(mode);
						}

						QUESTION_FORM.dispose();
					}
					else
					{
						JOptionPane.showMessageDialog(QUESTION_FORM, "Incorrect Format", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		
		finishButton.addActionListener(new ActionListener()
			{
				/*public void actionPerformed(ActionEvent e)
				{
					new QuizForm();
					QUESTION_FORM.dispose();
				}*/
			
			public void actionPerformed(ActionEvent e)
			{
				if (!"".equals(questionField.getText()))
				{
					int num = 0;
					for (int x = 0; x < answerPanel.getComponentCount(); x += 1)
					{
						if (tfCheckBox.isSelected() || !"".equals(((JTextField)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText()))
						{
							num += 1;
							
							if (tfCheckBox.isSelected())
							{
								answerStrings.add(((JLabel)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText());
							}
							else
							{
								answerStrings.add(((JTextField)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText());
							}
							
							if (((JRadioButton)((JPanel)answerPanel.getComponent(x)).getComponent(0)).isSelected())
							{
								correctAnswer = answerStrings.get(x);
							}
						}
					}
					
					if (num < 2 || correctAnswer == null)
					{
						answerStrings.clear();
						correctAnswer = null;
						JOptionPane.showMessageDialog(QUESTION_FORM, "Incorrect Format", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					questionString = questionField.getText();
					Question question;
					
					if (tfCheckBox.isSelected())
					{
						question = new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, "1");
					}
					else
					{
						question = new Question(questionString, answerStrings, correctAnswer, pointValue, "1");
					}
					
					QuizManager.questions.add(question);
					
					if (QuizManager.questions.size() == Constants.MAX_QUESTIONS)
					{
						finishButton.doClick();
					}
					else
					{
						new TeacherQuizForm(mode);
					}

					QUESTION_FORM.dispose();
				}
				else
				{
					JOptionPane.showMessageDialog(QUESTION_FORM, "Incorrect Format", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
				}
			}
			});
		
		cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new TeacherQuizForm(mode);
					QUESTION_FORM.dispose();
				}
			});
		
		tfCheckBox.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (tfCheckBox.isSelected())
				{
					answerPanel.removeAll();
					numAnswers = 2;
					JPanel panel = new JPanel();
					panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
					JRadioButton radio = new JRadioButton();
					JLabel text = new JLabel("True");
					answerGroup.add(radio);
					panel.add(radio);
					panel.add(text);
					answerPanel.add(panel);
					
					JPanel panel2 = new JPanel();
					panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
					JRadioButton radio2 = new JRadioButton();
					JLabel text2 = new JLabel("False");
					answerGroup.add(radio2);
					panel2.add(radio2);
					panel2.add(text2);
					answerPanel.add(panel2);
					((JRadioButton)((JPanel)answerPanel.getComponent(0)).getComponent(0)).setSelected(true);
					answerButton.setVisible(false);
					QUESTION_FORM.pack();
				}
				else
				{
					answerPanel.removeAll();
					answerButton.doClick();
					answerButton.doClick();
					((JRadioButton)((JPanel)answerPanel.getComponent(0)).getComponent(0)).setSelected(true);
					answerButton.setVisible(true);
					QUESTION_FORM.pack();
				}
			}
		});
		
		answerButton.doClick();
		answerButton.doClick();
		((JRadioButton)((JPanel)answerPanel.getComponent(0)).getComponent(0)).setSelected(true);
		
		buttonPanel.add(answerButton);
		buttonPanel.add(questionButton);
		buttonPanel.add(cancelButton);
		buttonPanel.add(finishButton);
		superPanel.add(buttonPanel);
		
		this.addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					QUESTION_FORM.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
					cancelButton.doClick();
				}
			});
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public QuestionForm(int index, TeacherClassSelectForm.Mode mode)
	{
		questionString = null;
		correctAnswer = null;
		answerStrings = new LinkedList<String>();
		pointValue = 10;
		
		Question question = QuizManager.questions.get(index);
		
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		
		JPanel questionPanel = new JPanel();
		JLabel questionLabel = new JLabel("Question");
		JTextField questionField = new JTextField(question.getQuestionString());
		JLabel answerLabel = new JLabel("Answers");
		JPanel lolzPanel = new JPanel();
		JCheckBox tfCheckBox = new JCheckBox("True/False");
		
		lolzPanel.setLayout(new BoxLayout(lolzPanel, BoxLayout.X_AXIS));
		lolzPanel.add(answerLabel);
		lolzPanel.add(tfCheckBox);
		questionPanel.add(questionLabel);
		questionPanel.add(questionField);
		questionPanel.add(lolzPanel);
		questionPanel.setLayout(new BoxLayout(questionPanel, BoxLayout.Y_AXIS));
		superPanel.add(questionPanel);
		
		JPanel answerPanel = new JPanel();
		answerPanel.setLayout(new BoxLayout(answerPanel, BoxLayout.Y_AXIS));
		ButtonGroup answerGroup = new ButtonGroup();
		superPanel.add(answerPanel);
		
		JPanel buttonPanel = new JPanel();
		JButton answerButton = new JButton("Add Answer");
		JButton questionButton = new JButton("Finish");
		JButton cancelButton = new JButton("Cancel");
		
		answerButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						if (numAnswers == Constants.MAX_ANSWERS)
						{
							return;
						}
						
						numAnswers += 1;
						JPanel panel = new JPanel();
						panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
						JRadioButton radio = new JRadioButton();
						JTextField text = new JTextField();
						answerGroup.add(radio);
						panel.add(radio);
						panel.add(text);
						answerPanel.add(panel);
						pack();
					}
				});
		
		questionButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (!"".equals(questionField.getText()))
					{
						int num = 0;
						for (int x = 0; x < answerPanel.getComponentCount(); x += 1)
						{
							if (tfCheckBox.isSelected() || !"".equals(((JTextField)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText()))
							{
								num += 1;
								
								if (tfCheckBox.isSelected())
								{
									answerStrings.add(((JLabel)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText());
								}
								else
								{
									answerStrings.add(((JTextField)((JPanel)answerPanel.getComponent(x)).getComponent(1)).getText());
								}
								
								if (((JRadioButton)((JPanel)answerPanel.getComponent(x)).getComponent(0)).isSelected())
								{
									correctAnswer = answerStrings.get(x);
								}
							}
						}
						
						if (num < 2 || correctAnswer == null)
						{
							answerStrings.clear();
							correctAnswer = null;
							JOptionPane.showMessageDialog(QUESTION_FORM, "Incorrect Format", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						questionString = questionField.getText();
						Question question;
						
						if (tfCheckBox.isSelected())
						{
							question = new TFQuestion(questionString, answerStrings, correctAnswer, pointValue, "1");
						}
						else
						{
							question = new Question(questionString, answerStrings, correctAnswer, pointValue, "1");
						}
						
						QuizManager.questions.set(index, question);
						new TeacherQuizForm(mode);
						QUESTION_FORM.dispose();
					}
					else
					{
						JOptionPane.showMessageDialog(QUESTION_FORM, "Incorrect Format", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		
		cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new TeacherQuizForm(mode);
					QUESTION_FORM.dispose();
				}
			});
		
		tfCheckBox.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (tfCheckBox.isSelected())
				{
					answerPanel.removeAll();
					numAnswers = 2;
					JPanel panel = new JPanel();
					panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
					JRadioButton radio = new JRadioButton();
					JLabel text = new JLabel("True");
					answerGroup.add(radio);
					panel.add(radio);
					panel.add(text);
					answerPanel.add(panel);
					
					JPanel panel2 = new JPanel();
					panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
					JRadioButton radio2 = new JRadioButton();
					JLabel text2 = new JLabel("False");
					answerGroup.add(radio2);
					panel2.add(radio2);
					panel2.add(text2);
					answerPanel.add(panel2);
					((JRadioButton)((JPanel)answerPanel.getComponent(0)).getComponent(0)).setSelected(true);
					answerButton.setVisible(false);
					QUESTION_FORM.pack();
				}
				else
				{
					answerPanel.removeAll();
					answerButton.doClick();
					answerButton.doClick();
					((JRadioButton)((JPanel)answerPanel.getComponent(0)).getComponent(0)).setSelected(true);
					answerButton.setVisible(true);
					QUESTION_FORM.pack();
				}
			}
		});
		
		if (!(question instanceof TFQuestion))
		{
			for (int x = 0; x < question.getAnswers().size(); x += 1)
			{
				JPanel panel = new JPanel();
				panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
				JRadioButton radio = new JRadioButton();
				
				if (question.getCorrectAnswer().equals(question.getAnswers().get(x)))
				{
					radio.setSelected(true);
				}
				
				JTextField text = new JTextField(question.getAnswers().get(x));
				answerGroup.add(radio);
				panel.add(radio);
				panel.add(text);
				answerPanel.add(panel);
			}
		}
		else
		{
			tfCheckBox.doClick();
			
			if ("True".equals(question.getCorrectAnswer()))
			{
				((JRadioButton)((JPanel)answerPanel.getComponent(0)).getComponent(0)).setSelected(true);
			}
			else
			{
				((JRadioButton)((JPanel)answerPanel.getComponent(1)).getComponent(0)).setSelected(true);
			}
		}
		
		buttonPanel.add(answerButton);
		buttonPanel.add(cancelButton);
		buttonPanel.add(questionButton);
		superPanel.add(buttonPanel);
		
		this.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				QUESTION_FORM.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
				cancelButton.doClick();
			}
		});
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new QuestionForm(TeacherClassSelectForm.Mode.Create);
	}
}