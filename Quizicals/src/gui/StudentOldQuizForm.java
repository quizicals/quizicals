package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import background.Constants;
import background.Question;
import background.QuizManager;

public class StudentOldQuizForm extends JFrame
{
	private final StudentOldQuizForm STUDENT_OLD_QUIZ_FORM = this;
	
	public StudentOldQuizForm()
	{
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel questionPanel = new JPanel();
		questionPanel.setLayout(new GridLayout(0, 1));
		JPanel buttonPanel = new JPanel();
		JButton closeButton = new JButton("Close");
		int selectedIndex = 0;
		int questionNumber = 1;
		
		JPanel explainPanel = new JPanel();
		explainPanel.setLayout(new GridLayout(0, 1));
		JLabel scoreLabel = new JLabel();
		JLabel explainLabel1 = new JLabel("* = Correct Answer");
		JLabel explainLabel2 = new JLabel("- = Your Answer");
		explainPanel.add(scoreLabel);
		explainPanel.add(explainLabel1);
		explainPanel.add(explainLabel2);
		superPanel.add(explainPanel);
		
		int theirScore = 0;
		int totalScore = 0;
		
		for (Question q : QuizManager.questions)
		{
			totalScore += q.getPointValue();
			JPanel answerPanel = new JPanel();
			answerPanel.setLayout(new BoxLayout(answerPanel, BoxLayout.Y_AXIS));
			JLabel questionLabel = new JLabel("" + questionNumber + ".  " + q.getQuestionString());
			answerPanel.add(questionLabel);
			
			char answerLetter = 'a';
			for (int x = 0; x < q.getAnswers().size(); x += 1)
			{
				String text = "    ";
				String answer = q.getAnswers().get(x);
				
				if (answer.equals(q.getCorrectAnswer()))
				{
					text += "*";
				}
				
				if (answer.equals(QuizManager.selectedAnswers.get(selectedIndex)))
				{
					text += "-";
					if (text.contains("*"))
					{
						theirScore += q.getPointValue();
					}
				}
				
				while (text.length() < 7)
				{
					text += " ";
				}
				
				text += (char)(answerLetter + x) + ".  ";
				
				text += answer;
				
				JLabel answerLabel = new JLabel(text);
				answerPanel.add(answerLabel);
				
			}
			
			questionNumber += 1;
			selectedIndex += 1;
			questionPanel.add(answerPanel);
		}
		
		scoreLabel.setText("You scored " + theirScore + " out of " + totalScore + " points.");
		
		closeButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						QuizManager.clear();
						new StudentMenuForm();
						STUDENT_OLD_QUIZ_FORM.dispose();
					}
				});
		
		JScrollPane scrollPane = new JScrollPane(questionPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(60);
		superPanel.add(scrollPane);
		buttonPanel.add(closeButton);
		superPanel.add(buttonPanel);
		
		this.add(superPanel);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(new Dimension(300, 300));
        this.setTitle(Constants.TITLE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
	}
}