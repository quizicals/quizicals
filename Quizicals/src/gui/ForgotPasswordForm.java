package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import background.Constants;
import background.UserType;

public class ForgotPasswordForm extends JFrame
{
	private final ForgotPasswordForm FORGOT_PASSWORD_FORM = this;
	
	public ForgotPasswordForm(UserType userType, String username, String securityQuestion, String securityAnswer)
	{
		JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel questionPanel = new JPanel();
		JPanel answerPanel = new JPanel();
		answerPanel.setLayout(new BoxLayout(answerPanel, BoxLayout.X_AXIS));
		JPanel buttonPanel = new JPanel();
		JLabel questionLabel = new JLabel("Security Question:  " + securityQuestion);
		JLabel answerLabel = new JLabel("Answer:  ");
		JTextField answerField = new JTextField();
		JButton submitButton = new JButton("Submit");
		JButton cancelButton = new JButton("Cancel");
		
		submitButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						if (securityAnswer.equals(answerField.getText()))
						{
							new PasswordResetForm(userType, username);
							FORGOT_PASSWORD_FORM.dispose();
						}
						else
						{
							JOptionPane.showMessageDialog(FORGOT_PASSWORD_FORM, "Incorrect answer", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
						}
					}
				});
		
		cancelButton.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new LoginForm();
					FORGOT_PASSWORD_FORM.dispose();
				}
			});
		
		questionPanel.add(questionLabel);
		answerPanel.add(answerLabel);
		answerPanel.add(answerField);
		buttonPanel.add(submitButton);
		buttonPanel.add(cancelButton);
		
		superPanel.add(questionPanel);
		superPanel.add(answerPanel);
		superPanel.add(buttonPanel);
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		new ForgotPasswordForm(UserType.student, "mwaz", "What is love?", "yes");
	}
}