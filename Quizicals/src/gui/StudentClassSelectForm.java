package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import background.Constants;
import background.QuizManager;
import background.User;
import database.Database;

public class StudentClassSelectForm extends JFrame {
	private final StudentClassSelectForm STUDENT_CLASS_SELECT_FORM = this;
	
    public StudentClassSelectForm(StudentQuizSelectForm.Mode mode) {
        // TODO Auto-generated constructor stub

       JPanel superPanel = new JPanel();
		superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
		JPanel labelPanel = new JPanel();
		labelPanel.add(new JLabel("Select a Class"));
		superPanel.add(labelPanel);
		
		Database db = Database.getInstance();
		ResultSet rs = db.query("SELECT * FROM classes ");
		
		try
		{
			JPanel classesPanel = new JPanel();
			classesPanel.setLayout(new GridLayout(0, 1));
			
			while (rs.next())
			{
				//don't display classes that the student isn't enrolled in
				ResultSet enrolled = db.query("SELECT enrollmentID FROM enrollment WHERE classID=" + rs.getInt("classID") + " AND studentID=" + User.getCurrentUser().getUID());
				if(!enrolled.next())
				{
					enrolled.close();
					continue;
				}
				enrolled.close();
				
				final JClassButton classButton = new JClassButton(rs.getString("className"), rs.getInt("classID"));
				
				classButton.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							QuizManager.classID = classButton.ID;
						   new StudentQuizSelectForm(mode);
                dispose();
						}
					});
				
				classesPanel.add(classButton);
			}
			
			rs.close();
			JScrollPane scrollPane = new JScrollPane(classesPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			superPanel.add(scrollPane);
		}
		catch (Exception e)
		{
			
		}
		
		db.close();
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		JButton backButton = new JButton("Back");
		
		backButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						new StudentMenuForm();
						STUDENT_CLASS_SELECT_FORM.dispose();
					}
				});
		
		buttonPanel.add(backButton);
		superPanel.add(buttonPanel);
		
		this.add(superPanel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(new Dimension(300, 300));
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
