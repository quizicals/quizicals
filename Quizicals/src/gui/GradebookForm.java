package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import background.Constants;
import background.FileIO;
import background.QuizScore;
import background.Student;
import background.User;
import background.UserType;
import database.Database;

public class GradebookForm extends JFrame implements ActionListener	
{
	private final GradebookForm GRADEBOOK_FORM = this;
	private ClassPair currentClass;
	private ArrayList<ClassPair> classes;
	private ArrayList<JIDTextField> textFields;
	private ArrayList<Integer> originalScores;
	private ArrayList<Student> students;
	private static final String NO_QUIZ_SCORE = "-";
	//private JPanel gradePanel;
	private JScrollPane scrollPane;

	private class ClassPair
	{
		private int id;
		private String name;
		
		public ClassPair(int classID, String className)
		{
			this.id = classID;
			this.name = className;
		}
		
		public int getID()
		{
			return this.id;
		}
		
		public String getName()
		{
			return this.name;
		}
		
		public String toString()
		{
			return this.name;
		}
		
		public boolean equals(ClassPair other)
		{
			return this.id == other.id;
		}
	}
	
	private class QuizPair
	{
		private int id;
		private String name;
		
		public QuizPair(int quizID, String quizName)
		{
			this.id = quizID;
			this.name = quizName;
		}
		
		public int getID()
		{
			return this.id;
		}
		
		public String getName()
		{
			return this.name;
		}
		
		public String toString()
		{
			return this.name;
		}
	}
	
	public GradebookForm()
	{
		//setup frame
		this.setLayout(new BorderLayout());
		
		//get classes
		this.classes = new ArrayList<ClassPair>();
		this.textFields = new ArrayList<JIDTextField>();
		this.students = new ArrayList<Student>();
		this.originalScores = new ArrayList<Integer>();
		this.scrollPane = null;
		Database db = Database.getInstance();
		User u = User.getCurrentUser();
		try
		{
			ResultSet results = db.query("SELECT * FROM classes WHERE teacherID=" + u.getUID());
			while(results.next())
			{
				int id = results.getInt("classID");
				String name = results.getString("className");
				
				this.classes.add(new ClassPair(id, name));
			}
			
			//no classes were found
			if(this.classes.size() == 0)
			{
				JOptionPane.showMessageDialog(this, "Could not find any classes for this user.", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
				new TeacherMenuForm();
				this.dispose();
			}
			results.close();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Quizicals has encountered an error", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
			new TeacherMenuForm();
			this.dispose();
		}
		this.currentClass = this.classes.get(0);
		db.close();
		
		//lower panel
		JPanel bottom = new JPanel();
		
		//create ComboBox
		JComboBox classSelector = new JComboBox(this.classes.toArray());
		classSelector.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent select)
				{
					//get selected class
					JComboBox selector = (JComboBox)select.getSource();
					ClassPair selected = (ClassPair)selector.getSelectedItem();
					
					//same class was selected
					if(GRADEBOOK_FORM.currentClass.getID() == selected.getID())
					{
						return;
					}
					
					//update grades if necessary
					selector.setVisible(false);
					selector.setVisible(true);
					
					if (hasChanges())
					{
						int result = JOptionPane.showConfirmDialog(GRADEBOOK_FORM, "Would you like to save your changes to the gradebook?", Constants.TITLE, JOptionPane.YES_NO_OPTION);
						if (result == JOptionPane.YES_OPTION)
						{
							GRADEBOOK_FORM.actionPerformed(select);
						}
						else if (result == JOptionPane.CLOSED_OPTION)
						{
							for (int x = 0; x < classSelector.getItemCount(); x += 1)
							{
								if (classSelector.getItemAt(x).equals(GRADEBOOK_FORM.currentClass))
								{
									classSelector.setSelectedIndex(x);
									break;
								}
							}
							return;
						}
					}
					
					//change the current class
					GRADEBOOK_FORM.currentClass = selected;
					setGradePanel();
					GRADEBOOK_FORM.repaint();
				}
			});
		
		//make bottom panel
		JPanel buttonPanel = new JPanel();
		JButton back = new JButton("Back");
		JButton update = new JButton("Update Gradebook");
		
		//add action listeners
		back.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent back)
				{
					if (hasChanges())
					{
						int result = JOptionPane.showConfirmDialog(GRADEBOOK_FORM, "Would you like to save your changes to the gradebook?", Constants.TITLE, JOptionPane.YES_NO_OPTION);
						if (result == JOptionPane.YES_OPTION)
						{
							GRADEBOOK_FORM.actionPerformed(new ActionEvent(new Object(), 0, ""));
						}
						else if (result == JOptionPane.CLOSED_OPTION)
						{
							return;
						}
					}

					new TeacherMenuForm();
					GRADEBOOK_FORM.dispose();
				}
			});
		
		update.addActionListener(this);
		
		setGradePanel();
		
		//add buttons to buttonPanel
		buttonPanel.add(back);
		buttonPanel.add(update);
		
		//insert bottom panel
		bottom.add(buttonPanel);
		bottom.add(classSelector);
		this.add(bottom, BorderLayout.PAGE_END);
		
		this.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				if (hasChanges())
				{
					int result = JOptionPane.showConfirmDialog(GRADEBOOK_FORM, "Would you like to save your changes to the gradebook?", Constants.TITLE, JOptionPane.YES_NO_OPTION);
					if (result == JOptionPane.YES_OPTION)
					{
						GRADEBOOK_FORM.actionPerformed(new ActionEvent(new Object(), 0, ""));
					}
					else if (result == JOptionPane.CLOSED_OPTION)
					{
						GRADEBOOK_FORM.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
						return;
					}
				}
				
				GRADEBOOK_FORM.dispose();
			}
		});
		
		//make frame visible
		this.setSize(new Dimension(900, 600));
		this.setTitle(Constants.TITLE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
	}

	@Override
	public void actionPerformed(ActionEvent update)
	{
		for (JIDTextField f : this.textFields)
		{
			String text = f.getText();
			
			if (NO_QUIZ_SCORE.equals(text))
			{
				continue;
			}
			
			try
			{
				Integer.parseInt(text);
			}
			catch (NumberFormatException e)
			{
				JOptionPane.showMessageDialog(GRADEBOOK_FORM, "Invalid score(s)", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		
		Database database = Database.getInstance();
		
		for (int x = 0; x < this.originalScores.size(); x += 1)
		{
			int score;
			
			if (NO_QUIZ_SCORE.equals(this.textFields.get(x).getText()) && this.originalScores.get(x) >= 0)
			{
				score = -1;
			}
			else
			{
				if (this.NO_QUIZ_SCORE.equals(this.textFields.get(x).getText()))
				{
					score = -1;
				}
				else
				{
					score = Integer.parseInt(this.textFields.get(x).getText());
				}
			}
			
			if (this.originalScores.get(x) != score)
			{
				try
				{
					if (this.originalScores.get(x) == -1 && score != -1)
					{
						ResultSet rs = database.query("SELECT enrollmentID FROM enrollment WHERE studentID=" + this.textFields.get(x).studentID + " AND classID=" + this.currentClass.getID());
						rs.next();
						
						database.query("INSERT INTO scores (quizID, enrollmentID, studentID, score) VALUES (" + this.textFields.get(x).quizID + ", " + rs.getInt("enrollmentID") + ", " + this.textFields.get(x).studentID + ", " + score + ")");
						rs.close();
					}
					else if (score != -1)
					{
						database.query("UPDATE scores SET score=" + score + " WHERE quizId=" + this.textFields.get(x).quizID + " AND studentID=" + this.textFields.get(x).studentID);
					}
					else
					{
						database.query("DELETE FROM scores WHERE quizId=" + this.textFields.get(x).quizID + " AND studentID=" + this.textFields.get(x).studentID);
						FileIO.delete(currentClass.getID() + "/" + this.textFields.get(x).quizID + "/" + this.textFields.get(x).studentID + ".cq");
					}
					
					this.originalScores.set(x, score);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		
		database.close();
	}
	
	private void setGradePanel()
	{
		if(this.scrollPane != null)
		{
			this.remove(this.scrollPane);
			this.students.clear();
			this.textFields.clear();
			this.originalScores.clear();
		}
		
		Database database = Database.getInstance();
		try
		{
			ResultSet enrollmentRS = database.query("SELECT enrollmentID, studentID FROM enrollment WHERE classID=" + this.currentClass.id);
			ResultSet quizRS = database.query("SELECT quizID, quizName FROM quizzes WHERE classID=" + this.currentClass.id);
			ArrayList<QuizPair> quizPairs = new ArrayList<QuizPair>();
			
			while (quizRS.next())
			{
				quizPairs.add(new QuizPair(quizRS.getInt("quizID"), quizRS.getString("quizName")));
			}

			while (enrollmentRS.next())
			{
				ResultSet studentRS = database.query("SELECT firstName, lastName, studentID FROM students WHERE studentID=" + enrollmentRS.getInt("studentID"));
				studentRS.next();
				ResultSet allScores = database.query("SELECT score, quizID, enrollmentID FROM scores WHERE enrollmentID=" + enrollmentRS.getInt("enrollmentID") + " ORDER BY quizID ASC");
				
				int studentID = enrollmentRS.getInt("studentID");
				String name = studentRS.getString("lastName") + ", " + studentRS.getString("firstName");
				ArrayList<QuizScore> quizScores = new ArrayList<QuizScore>();
				int scoreIndex = 0;
				
				while (allScores.next())
				{
					while (allScores.getInt("quizID") != quizPairs.get(scoreIndex).getID())
					{
						quizScores.add(null);
						this.textFields.add(new JIDTextField(this.NO_QUIZ_SCORE, quizPairs.get(scoreIndex).getID(), enrollmentRS.getInt("studentID")));
						scoreIndex += 1;
					}
					
					String quizName = quizPairs.get(scoreIndex).getName();
					int points = allScores.getInt("score");
					
					quizScores.add(new QuizScore(quizName, points));
					this.textFields.add(new JIDTextField(allScores.getString("score"), quizPairs.get(scoreIndex).getID(), enrollmentRS.getInt("studentID")));
					
					scoreIndex += 1;
				}
				
				while(scoreIndex < quizPairs.size())
				{
					quizScores.add(null);
					this.textFields.add(new JIDTextField(this.NO_QUIZ_SCORE, quizPairs.get(scoreIndex).getID(), enrollmentRS.getInt("studentID")));
					scoreIndex += 1;
				}
				
				this.students.add(new Student(studentID, name, quizScores));
				studentRS.close();
				allScores.close();
			}
			
			quizRS.close();
			enrollmentRS.close();
			
			//TODO:  Display info in window
			JPanel gradePanel = new JPanel();
			gradePanel.setLayout(new GridLayout(students.size() + 1, quizPairs.size() + 1));

			gradePanel.add(new JLabel());
			
			for (QuizPair qp : quizPairs)
			{
				gradePanel.add(new JLabel(qp.getName()));
			}
			
			int index = 0;
			for (int x = 0; x < this.students.size(); x += 1)
			{
				gradePanel.add(new JLabel(this.students.get(x).getName()));
				
				for (int y = 0; y < quizPairs.size(); y += 1)
				{
					gradePanel.add(this.textFields.get(index));
					
					if (this.NO_QUIZ_SCORE.equals(this.textFields.get(index).getText()))
					{
						this.originalScores.add(-1);
					}
					else
					{
						this.originalScores.add(Integer.parseInt(this.textFields.get(index).getText()));
					}
					
					index += 1;
				}
			}
			
			this.scrollPane = new JScrollPane(gradePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.scrollPane.getVerticalScrollBar().setUnitIncrement(10);
			this.scrollPane.getHorizontalScrollBar().setUnitIncrement(4);
			this.add(this.scrollPane);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		database.close();
	}
	
	private boolean hasChanges()
	{
		boolean changed = false;
		
		for (int x = 0; x < this.originalScores.size(); x += 1)
		{
			int score;
			
			if (this.NO_QUIZ_SCORE.equals(this.textFields.get(x).getText()))
			{
				score = -1;
			}
			else
			{
				try
				{
					score = Integer.parseInt(this.textFields.get(x).getText());
				}
				catch (Exception e)
				{
					changed = true;
					break;
				}
			}
			
			if (this.originalScores.get(x) != score)
			{
				changed = true;
				break;
			}
		}
		
		return changed;
	}
	
	public static void main(String[] args)
	{
		Database database = Database.getInstance();
		try
		{
			for (int x = 0; x < 100; x += 1)
			{
				ResultSet rs = database.query("SELECT * FROM students WHERE firstName=\'" + x + "\' AND lastName='Ross' AND username=\'" + x + "\'");
	            if(!rs.next())
	            {
	            	rs.close();
	            	rs = database.query("INSERT INTO students (firstName, lastName, username, password, securityQuestion, securityAnswer) VALUES (\'" + x + "\', 'Ross', \'" + x + "\', 'password', 'Is that a happy little tree?', 'yes')");
	            	rs = database.query("SELECT studentID FROM students WHERE firstName=\'" + x + "\' AND lastName='Ross' AND username=\'" + x + "\'");
	            	rs.next();
	            	int studentID = rs.getInt("studentID");
	            	rs.close();
	            	rs = database.query("SELECT classID FROM classes WHERE className='The Joy of Painting'");
	            	rs.next();
	            	int classID = rs.getInt("classID");
	            	rs.close();
	            	
	            	rs = database.query("SELECT * FROM quizzes WHERE quizName=\'" + x + "\' AND classID=" + classID);
	            	if (!rs.next())
	            	{
	            		rs.close();
	            		rs = database.query("INSERT INTO quizzes (quizName, classID) VALUES (\'" + x + "\', " + classID + ")");
	            	}
	            	
	            	rs = database.query("INSERT INTO enrollment (studentID, classID) VALUES (" + studentID + ", " + classID + ")");
	            }
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		database.close();
		
		User.setCurrentUser(UserType.teacher, "Bob", "Ross", "bross", 1);
		new GradebookForm();
	}
}
