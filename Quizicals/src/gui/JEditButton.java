package gui;

import javax.swing.JButton;

public class JEditButton extends JButton
{
	public final int INDEX;
	
	public JEditButton(int index)
	{
		super("Edit");
		this.INDEX = index;
	}
}
