/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.ResultSet;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import background.CompletedQuiz;
import background.Constants;
import background.FileIO;
import background.Quiz;
import background.QuizManager;
import background.User;
import database.Database;

/**
 *
 * @author asad
 */
public class StudentQuizSelectForm extends JFrame {

    private final StudentQuizSelectForm QUIZ_SELECT_FORM = this;
    public enum Mode {New, Old};
    
    public StudentQuizSelectForm(Mode mode) {
        JPanel superPanel = new JPanel();
        superPanel.setLayout(new BoxLayout(superPanel, BoxLayout.Y_AXIS));
        JPanel labelPanel = new JPanel();
        labelPanel.add(new JLabel("Select a Quiz"));
        superPanel.add(labelPanel);

        Database db = Database.getInstance();
        ResultSet rs = db.query("SELECT * FROM quizzes WHERE classID=\'" + QuizManager.classID + "\'");

        try {
            JPanel quizzesPanel = new JPanel();
            quizzesPanel.setLayout(new GridLayout(0, 1));

            while (rs.next())
            {
            	//do not let the user retake a quiz
            	ResultSet scores = db.query("SELECT scoreID FROM scores WHERE quizID=" + rs.getInt("quizID") + " AND studentID=" + User.getCurrentUser().getUID());
            	
            	if (mode == Mode.New)
            	{
	            	if(scores.next())
	            	{
	            		scores.close();
	            		continue;
	            	}
	            	else if (!FileIO.exists(QuizManager.classID + "/" + rs.getInt("quizID") + ".q"))
	            	{
	            		scores.close();
	            		continue;
	            	}
            	}
            	else
            	{
            		if(!scores.next())
	            	{
	            		scores.close();
	            		continue;
	            	}
            		else if (!FileIO.exists(QuizManager.classID + "/" + rs.getInt("quizID") + "/" + User.getCurrentUser().getUID() + ".cq"))
            		{
            			scores.close();
	            		continue;
            		}
            	}
            	
            	scores.close();
            	
                final JQuizButton quizButton = new JQuizButton(rs.getString("quizName"), rs.getInt("quizID"));

                quizButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                    	Quiz quiz;
                    	
                    	if (mode == Mode.New)
                    	{
                    		quiz = (Quiz)FileIO.read(QuizManager.classID + "/" + quizButton.ID + ".q");
                    	}
                    	else
                    	{
                    		quiz = (Quiz)FileIO.read(QuizManager.classID + "/" + quizButton.ID + "/" + User.getCurrentUser().getUID() + ".cq");
                    		QuizManager.selectedAnswers = ((CompletedQuiz)quiz).getSelectedAnswers();
                    	}
                    	
		        		QuizManager.questions = quiz.getQuestions();
		        		QuizManager.quizInfo = quiz.getQuizInformation();
                    	QuizManager.quizID = quizButton.ID;
                    	
                    	if (mode == Mode.New)
                    	{
                    		new StudentQuizForm();
                    	}
                    	else
                    	{
                    		new StudentOldQuizForm();
                    	}
                        dispose();
                    }
                });

                quizzesPanel.add(quizButton);
            }

            rs.close();
            JScrollPane scrollPane = new JScrollPane(quizzesPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            superPanel.add(scrollPane);
        } catch (Exception e) {

        }
        db.close();

        JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		JButton backButton = new JButton("Back");
		
		backButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						QuizManager.classID = -1;
						new StudentClassSelectForm(mode);
						QUIZ_SELECT_FORM.dispose();
					}
				});
		
		buttonPanel.add(backButton);
		superPanel.add(buttonPanel);
        
        this.add(superPanel);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(new Dimension(300, 300));
        this.setTitle(Constants.TITLE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
