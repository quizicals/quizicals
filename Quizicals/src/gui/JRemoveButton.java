package gui;

import javax.swing.JButton;

/**
 * @author Andrew Sytsma <asytsma@purdue.edu>
 */
public class JRemoveButton extends JButton
{
	public final int INDEX;
	
	public JRemoveButton(int index)
	{
		super("Remove");
		this.INDEX = index;
	}
}