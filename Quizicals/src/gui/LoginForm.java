package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import background.Constants;
import background.Properties;
import background.User;
import background.UserType;
import database.Database;

public class LoginForm {
    public LoginForm() {
        // TODO Auto-generated constructor stub
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("No such UI");
        }
        final JFrame frame = new JFrame(Constants.TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setLayout(new GridLayout(0, 1));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


        JLabel userName = new JLabel("Username: ");
        final JTextField userName_Field = new JTextField(20);
        JLabel passWord = new JLabel("Password: ");
        final JPasswordField passwordField = new JPasswordField(20);

        JButton forgotButton = new JButton("Forgot password?");
        JButton teachLoginButton = new JButton("Log in with teacher");
        JButton studentLoginButton = new JButton("Log in with student");

        forgotButton.addActionListener(new ActionListener()
        		{
        			public void actionPerformed(ActionEvent e)
        			{
        				Database database = Database.getInstance();
        				ResultSet studentRS = database.query("SELECT securityQuestion, securityAnswer FROM students WHERE username=\'" + userName_Field.getText() + "\'");
        				UserType userType;
        				String securityQuestion;
        				String securityAnswer;
        				
        				try
        				{
	        				if (studentRS.next())
	        				{
	        					userType = UserType.student;
	        					securityQuestion = studentRS.getString("securityQuestion");
	        					securityAnswer = studentRS.getString("securityAnswer");
	        					studentRS.close();
	        				}
	        				else
	        				{
	        					studentRS.close();
	        					
	        					ResultSet teacherRS = database.query("SELECT securityQuestion, securityAnswer FROM teachers WHERE username=\'" + userName_Field.getText() + "\'");
	        					
	        					if (teacherRS.next())
	        					{
	        						userType = UserType.teacher;
		        					securityQuestion = teacherRS.getString("securityQuestion");
		        					securityAnswer = teacherRS.getString("securityAnswer");
		        					teacherRS.close();
	        					}
	        					else
	        					{
	        						JOptionPane.showMessageDialog(frame, "Username does not exist", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
	        						return;
	        					}
	        				}
        				}
        				catch (Exception ex)
        				{
        					return;
        				}
        				
        				database.close();
        				
        				new ForgotPasswordForm(userType, userName_Field.getText(), securityQuestion, securityAnswer);
        				frame.dispose();
        			}
        		});
        
        teachLoginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	//check the teacher's credentials
            	Database database = Database.getInstance();
            	ResultSet rs = database.query("SELECT * FROM teachers WHERE username=\'" + userName_Field.getText() + "\' AND password=\'" + passwordField.getText() + "\';");
            	try
            	{
            		if (!rs.next())
            		{
            			JOptionPane.showMessageDialog(frame, "Invalid Username/Password", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
            			passwordField.setText("");
            			return;
            		}
            		else
            		{
            			//do teacher stuff
            			UserType type = UserType.teacher;
            			String firstName = rs.getString("firstName");
            			String lastName = rs.getString("lastName");
            			String username = rs.getString("username");
            			int uid = rs.getInt("teacherID");
            			
            			User.setCurrentUser(type, firstName, lastName, username, uid);
            			rs.close();
            		}
            	}
            	catch (SQLException exception)
            	{
            		
            	}
            	
            	database.close();
            	
                // TODO Auto-generated method stub
            	new TeacherMenuForm();
                frame.dispose();
            }
        });

        studentLoginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            	//check the student's credentials
            	Database database = Database.getInstance();
            	ResultSet rs = database.query("SELECT * FROM students WHERE username=\'" + userName_Field.getText() + "\' AND password=\'" + passwordField.getText() + "\';");
            	try
            	{
            		if (!rs.next())
            		{
            			JOptionPane.showMessageDialog(frame, "Invalid Username/Password", Constants.TITLE, JOptionPane.ERROR_MESSAGE);
            			passwordField.setText("");
            			return;
            		}
            		else
            		{
            			//do student stuff
            			UserType type = UserType.student;
            			String firstName = rs.getString("firstName");
            			String lastName = rs.getString("lastName");
            			String username = rs.getString("username");
            			int uid = rs.getInt("studentID");
            			
            			User.setCurrentUser(type, firstName, lastName, username, uid);
            		}
            	}
            	catch (SQLException exception)
            	{
            		
            	}
            	
                // TODO Auto-generated method stub
                new StudentMenuForm();
                frame.dispose();
            }
        });


        //=====================Add components here=====================//
        frame.add(userName);
        frame.add(userName_Field);
        frame.add(passWord);
        frame.add(passwordField);
        frame.add(forgotButton);
        frame.add(teachLoginButton);
        frame.add(studentLoginButton);

    }

    public static void main(String[] args)
    {
    	if (args.length == 1)
    	{
    		if ("-p".equals(args[0]))
    		{
    			Properties.main(null);
    		}
    		else if ("-q".equals(args[0]))
    		{
    			Database.main(null);
    		}
    		
    		return;
    	}
    	
        // TODO Auto-generated method stub

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                new LoginForm();
            }
        });
    }

}
